# README #

This SDK is used to access various endpoints defined [here](https://braango.api-docs.io/v2)

The link above consists of full documentation of braango system. 

### What is this repository for? ###

* This respository consists of fully implemented JAVA SDK that access the endpoints defined [here](https://braango.api-docs.io/v2)
* 1.0.0

### How do I get set up? ###
* To use this API, java 1.7 or above is required. Please add this dependency to your maven pom file


<dependency>
   <groupId>com.braango</groupId>
   <artifactId>braangosdk</artifactId>
   <version>1.0.0</version>
</dependency>


### Who do I talk to? ###

* support@braango.com