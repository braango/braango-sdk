/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangoapi;

import com.braango.client.ApiException;
import com.braango.client.braangomodel.BannersInput;
import com.braango.client.braangomodel.BannersOutput;
import com.braango.client.braangomodel.ErrorBadRequestWrapper;
import com.braango.client.braangomodel.ErrorBodyBadAuthorization;
import com.braango.client.braangomodel.ErrorResourceConflictWrapper;
import com.braango.client.braangomodel.ErrorResourceNotFoundWrapper;
import com.braango.client.braangomodel.ErrorResourceNotUpdatedWrapper;
import com.braango.client.braangomodel.ErrorUnprocessableRequestWrapper;
import com.braango.client.braangomodel.ErrorWrapper;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for BannersApi
 */
@Ignore
public class BannersApiTest {

    private final BannersApi api = new BannersApi();

    
    /**
     * Create banners
     *
     * Create list of banners. All three types of banners can be specified here, client, dealer or supervisor
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void createBannersTest() throws ApiException {
        String subdealerid = null;
        String salespersonid = null;
        BannersInput body = null;
        BannersOutput response = api.createBanners(subdealerid, salespersonid, body);

        // TODO: test validations
    }
    
    /**
     * Delete banners
     *
     * Delete banners by specifying banner type
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteBannersTest() throws ApiException {
        String subdealerid = null;
        String salespersonid = null;
        String bannertype = null;
        String apiKey = null;
        String accountType = null;
        BannersOutput response = api.deleteBanners(subdealerid, salespersonid, bannertype, apiKey, accountType);

        // TODO: test validations
    }
    
    /**
     * Get banners
     *
     * Get the current list of banners for given banner_type as specified in the URL
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getBannersTest() throws ApiException {
        String subdealerid = null;
        String salespersonid = null;
        String bannertype = null;
        String apiKey = null;
        String accountType = null;
        BannersOutput response = api.getBanners(subdealerid, salespersonid, bannertype, apiKey, accountType);

        // TODO: test validations
    }
    
}
