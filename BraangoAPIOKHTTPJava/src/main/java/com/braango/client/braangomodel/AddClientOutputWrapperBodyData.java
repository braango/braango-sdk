/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangomodel;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.UUID;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * AddClientOutputWrapperBodyData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-18T17:56:28.358-08:00")
public class AddClientOutputWrapperBodyData {
  @SerializedName("connect_id")
  private UUID connectId = null;

  @SerializedName("connect_enable")
  private Boolean connectEnable = null;

  public AddClientOutputWrapperBodyData connectId(UUID connectId) {
    this.connectId = connectId;
    return this;
  }

   /**
   * Get connectId
   * @return connectId
  **/
  @Valid
  @ApiModelProperty(value = "")
  public UUID getConnectId() {
    return connectId;
  }

  public void setConnectId(UUID connectId) {
    this.connectId = connectId;
  }

  public AddClientOutputWrapperBodyData connectEnable(Boolean connectEnable) {
    this.connectEnable = connectEnable;
    return this;
  }

   /**
   * Get connectEnable
   * @return connectEnable
  **/
  @ApiModelProperty(value = "")
  public Boolean getConnectEnable() {
    return connectEnable;
  }

  public void setConnectEnable(Boolean connectEnable) {
    this.connectEnable = connectEnable;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddClientOutputWrapperBodyData addClientOutputWrapperBodyData = (AddClientOutputWrapperBodyData) o;
    return Objects.equals(this.connectId, addClientOutputWrapperBodyData.connectId) &&
        Objects.equals(this.connectEnable, addClientOutputWrapperBodyData.connectEnable);
  }

  @Override
  public int hashCode() {
    return Objects.hash(connectId, connectEnable);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddClientOutputWrapperBodyData {\n");
    
    sb.append("    connectId: ").append(toIndentedString(connectId)).append("\n");
    sb.append("    connectEnable: ").append(toIndentedString(connectEnable)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

