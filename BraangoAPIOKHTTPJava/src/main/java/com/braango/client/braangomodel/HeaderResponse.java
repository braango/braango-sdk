/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangomodel;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.UUID;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * HeaderResponse
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-18T17:56:28.358-08:00")
public class HeaderResponse {
  @SerializedName("isn-request-id")
  private UUID isnRequestId = null;

  @SerializedName("id")
  private String id = null;

  public HeaderResponse isnRequestId(UUID isnRequestId) {
    this.isnRequestId = isnRequestId;
    return this;
  }

   /**
   * Get isnRequestId
   * @return isnRequestId
  **/
  @NotNull
  @Valid
  @ApiModelProperty(required = true, value = "")
  public UUID getIsnRequestId() {
    return isnRequestId;
  }

  public void setIsnRequestId(UUID isnRequestId) {
    this.isnRequestId = isnRequestId;
  }

  public HeaderResponse id(String id) {
    this.id = id;
    return this;
  }

   /**
   * This value is reflection for POST and PUT method&#39;s id value in header if specified. If value is null in those requests, this value will be always be current session id. For GET and DELETE it will always be session id. This value is not cached into the braango server
   * @return id
  **/
  @ApiModelProperty(value = "This value is reflection for POST and PUT method's id value in header if specified. If value is null in those requests, this value will be always be current session id. For GET and DELETE it will always be session id. This value is not cached into the braango server")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HeaderResponse headerResponse = (HeaderResponse) o;
    return Objects.equals(this.isnRequestId, headerResponse.isnRequestId) &&
        Objects.equals(this.id, headerResponse.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isnRequestId, id);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HeaderResponse {\n");
    
    sb.append("    isnRequestId: ").append(toIndentedString(isnRequestId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

