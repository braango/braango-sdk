/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangomodel;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * WebhookInputBody
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-18T17:56:28.358-08:00")
public class WebhookInputBody {
  @SerializedName("enable")
  private Boolean enable = null;

  @SerializedName("post_url")
  private String postUrl = null;

  @SerializedName("auth_key")
  private String authKey = null;

  @SerializedName("auth_id")
  private String authId = null;

  public WebhookInputBody enable(Boolean enable) {
    this.enable = enable;
    return this;
  }

   /**
   * Get enable
   * @return enable
  **/
  @NotNull
  @ApiModelProperty(required = true, value = "")
  public Boolean getEnable() {
    return enable;
  }

  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  public WebhookInputBody postUrl(String postUrl) {
    this.postUrl = postUrl;
    return this;
  }

   /**
   * Post back URL. Braango will create POST request to this URL 
   * @return postUrl
  **/
  @NotNull
  @ApiModelProperty(required = true, value = "Post back URL. Braango will create POST request to this URL ")
  public String getPostUrl() {
    return postUrl;
  }

  public void setPostUrl(String postUrl) {
    this.postUrl = postUrl;
  }

  public WebhookInputBody authKey(String authKey) {
    this.authKey = authKey;
    return this;
  }

   /**
   * API Key or token to use to validate requests. Braango will use this as part of header in its web-hook request. Please see &lt;&gt; for the web hook format
   * @return authKey
  **/
  @NotNull
  @ApiModelProperty(required = true, value = "API Key or token to use to validate requests. Braango will use this as part of header in its web-hook request. Please see <> for the web hook format")
  public String getAuthKey() {
    return authKey;
  }

  public void setAuthKey(String authKey) {
    this.authKey = authKey;
  }

  public WebhookInputBody authId(String authId) {
    this.authId = authId;
    return this;
  }

   /**
   * ID identifying this resource
   * @return authId
  **/
  @NotNull
  @ApiModelProperty(required = true, value = "ID identifying this resource")
  public String getAuthId() {
    return authId;
  }

  public void setAuthId(String authId) {
    this.authId = authId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WebhookInputBody webhookInputBody = (WebhookInputBody) o;
    return Objects.equals(this.enable, webhookInputBody.enable) &&
        Objects.equals(this.postUrl, webhookInputBody.postUrl) &&
        Objects.equals(this.authKey, webhookInputBody.authKey) &&
        Objects.equals(this.authId, webhookInputBody.authId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(enable, postUrl, authKey, authId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WebhookInputBody {\n");
    
    sb.append("    enable: ").append(toIndentedString(enable)).append("\n");
    sb.append("    postUrl: ").append(toIndentedString(postUrl)).append("\n");
    sb.append("    authKey: ").append(toIndentedString(authKey)).append("\n");
    sb.append("    authId: ").append(toIndentedString(authId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

