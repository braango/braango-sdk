/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangomodel;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * Payload
 */
@ApiModel(description = "Payload")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-18T17:56:28.358-08:00")
public class AccountCreateOutputWrapperBodyData {
  @SerializedName("dealer_pk_id")
  private Integer dealerPkId = null;

  @SerializedName("dealer_api_key")
  private String dealerApiKey = null;

  @SerializedName("partner_api_key")
  private String partnerApiKey = null;

  @SerializedName("dealer_id")
  private String dealerId = null;

  public AccountCreateOutputWrapperBodyData dealerPkId(Integer dealerPkId) {
    this.dealerPkId = dealerPkId;
    return this;
  }

   /**
   * Integer version of internal ID for this partner_dealer or real_dealer
   * @return dealerPkId
  **/
  @ApiModelProperty(value = "Integer version of internal ID for this partner_dealer or real_dealer")
  public Integer getDealerPkId() {
    return dealerPkId;
  }

  public void setDealerPkId(Integer dealerPkId) {
    this.dealerPkId = dealerPkId;
  }

  public AccountCreateOutputWrapperBodyData dealerApiKey(String dealerApiKey) {
    this.dealerApiKey = dealerApiKey;
    return this;
  }

   /**
   * API Key for the dealer signed up. Necessary to use this key to accessing resources for this particular account.
   * @return dealerApiKey
  **/
  @NotNull
  @ApiModelProperty(required = true, value = "API Key for the dealer signed up. Necessary to use this key to accessing resources for this particular account.")
  public String getDealerApiKey() {
    return dealerApiKey;
  }

  public void setDealerApiKey(String dealerApiKey) {
    this.dealerApiKey = dealerApiKey;
  }

  public AccountCreateOutputWrapperBodyData partnerApiKey(String partnerApiKey) {
    this.partnerApiKey = partnerApiKey;
    return this;
  }

   /**
   * Get partnerApiKey
   * @return partnerApiKey
  **/
  @ApiModelProperty(value = "")
  public String getPartnerApiKey() {
    return partnerApiKey;
  }

  public void setPartnerApiKey(String partnerApiKey) {
    this.partnerApiKey = partnerApiKey;
  }

  public AccountCreateOutputWrapperBodyData dealerId(String dealerId) {
    this.dealerId = dealerId;
    return this;
  }

   /**
   * Internal dealer id generated by braango
   * @return dealerId
  **/
  @NotNull
  @ApiModelProperty(required = true, value = "Internal dealer id generated by braango")
  public String getDealerId() {
    return dealerId;
  }

  public void setDealerId(String dealerId) {
    this.dealerId = dealerId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountCreateOutputWrapperBodyData accountCreateOutputWrapperBodyData = (AccountCreateOutputWrapperBodyData) o;
    return Objects.equals(this.dealerPkId, accountCreateOutputWrapperBodyData.dealerPkId) &&
        Objects.equals(this.dealerApiKey, accountCreateOutputWrapperBodyData.dealerApiKey) &&
        Objects.equals(this.partnerApiKey, accountCreateOutputWrapperBodyData.partnerApiKey) &&
        Objects.equals(this.dealerId, accountCreateOutputWrapperBodyData.dealerId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dealerPkId, dealerApiKey, partnerApiKey, dealerId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountCreateOutputWrapperBodyData {\n");
    
    sb.append("    dealerPkId: ").append(toIndentedString(dealerPkId)).append("\n");
    sb.append("    dealerApiKey: ").append(toIndentedString(dealerApiKey)).append("\n");
    sb.append("    partnerApiKey: ").append(toIndentedString(partnerApiKey)).append("\n");
    sb.append("    dealerId: ").append(toIndentedString(dealerId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

