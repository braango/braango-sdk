/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangoapi;

import com.braango.client.ApiCallback;
import com.braango.client.ApiClient;
import com.braango.client.ApiException;
import com.braango.client.ApiResponse;
import com.braango.client.Configuration;
import com.braango.client.Pair;
import com.braango.client.ProgressRequestBody;
import com.braango.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import javax.validation.constraints.*;

import com.braango.client.braangomodel.BannersInput;
import com.braango.client.braangomodel.BannersOutput;
import com.braango.client.braangomodel.ErrorBadRequestWrapper;
import com.braango.client.braangomodel.ErrorBodyBadAuthorization;
import com.braango.client.braangomodel.ErrorResourceConflictWrapper;
import com.braango.client.braangomodel.ErrorResourceNotFoundWrapper;
import com.braango.client.braangomodel.ErrorResourceNotUpdatedWrapper;
import com.braango.client.braangomodel.ErrorUnprocessableRequestWrapper;
import com.braango.client.braangomodel.ErrorWrapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BannersApi {
    private ApiClient apiClient;

    public BannersApi() {
        this(Configuration.getDefaultApiClient());
    }

    public BannersApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for createBanners
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createBannersCall(String subdealerid, String salespersonid, BannersInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;
        
        // create path and map variables
        String localVarPath = "/personnel/banners/{subdealerid}/{salespersonid}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createBannersValidateBeforeCall(String subdealerid, String salespersonid, BannersInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling createBanners(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling createBanners(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createBannersCall(subdealerid, salespersonid, body, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Create banners
     * Create list of banners. All three types of banners can be specified here, client, dealer or supervisor
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @return BannersOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BannersOutput createBanners(String subdealerid, String salespersonid, BannersInput body) throws ApiException {
        ApiResponse<BannersOutput> resp = createBannersWithHttpInfo(subdealerid, salespersonid, body);
        return resp.getData();
    }

    /**
     * Create banners
     * Create list of banners. All three types of banners can be specified here, client, dealer or supervisor
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @return ApiResponse&lt;BannersOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BannersOutput> createBannersWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  BannersInput body) throws ApiException {
        com.squareup.okhttp.Call call = createBannersValidateBeforeCall(subdealerid, salespersonid, body, null, null);
        Type localVarReturnType = new TypeToken<BannersOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create banners (asynchronously)
     * Create list of banners. All three types of banners can be specified here, client, dealer or supervisor
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createBannersAsync(String subdealerid, String salespersonid, BannersInput body, final ApiCallback<BannersOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createBannersValidateBeforeCall(subdealerid, salespersonid, body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BannersOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteBanners
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteBannersCall(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/personnel/banners/{subdealerid}/{salespersonid}/{bannertype}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()))
            .replaceAll("\\{" + "bannertype" + "\\}", apiClient.escapeString(bannertype.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (apiKey != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "api_key", apiKey));
        if (accountType != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "account_type", accountType));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteBannersValidateBeforeCall(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling deleteBanners(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling deleteBanners(Async)");
        }
        
        // verify the required parameter 'bannertype' is set
        if (bannertype == null) {
            throw new ApiException("Missing the required parameter 'bannertype' when calling deleteBanners(Async)");
        }
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling deleteBanners(Async)");
        }
        
        // verify the required parameter 'accountType' is set
        if (accountType == null) {
            throw new ApiException("Missing the required parameter 'accountType' when calling deleteBanners(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteBannersCall(subdealerid, salespersonid, bannertype, apiKey, accountType, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Delete banners
     * Delete banners by specifying banner type
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return BannersOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BannersOutput deleteBanners(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType) throws ApiException {
        ApiResponse<BannersOutput> resp = deleteBannersWithHttpInfo(subdealerid, salespersonid, bannertype, apiKey, accountType);
        return resp.getData();
    }

    /**
     * Delete banners
     * Delete banners by specifying banner type
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return ApiResponse&lt;BannersOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BannersOutput> deleteBannersWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  @NotNull @Pattern(regexp="^(?:client|dealer|supervisor)$") String bannertype,  @NotNull @Pattern(regexp="^(ISN)[A-za-z0-9]{3,}$") String apiKey,  @NotNull @Pattern(regexp="^(?:partner|dealer)$") String accountType) throws ApiException {
        com.squareup.okhttp.Call call = deleteBannersValidateBeforeCall(subdealerid, salespersonid, bannertype, apiKey, accountType, null, null);
        Type localVarReturnType = new TypeToken<BannersOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete banners (asynchronously)
     * Delete banners by specifying banner type
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteBannersAsync(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType, final ApiCallback<BannersOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteBannersValidateBeforeCall(subdealerid, salespersonid, bannertype, apiKey, accountType, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BannersOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBanners
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBannersCall(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/personnel/banners/{subdealerid}/{salespersonid}/{bannertype}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()))
            .replaceAll("\\{" + "bannertype" + "\\}", apiClient.escapeString(bannertype.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (apiKey != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "api_key", apiKey));
        if (accountType != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "account_type", accountType));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBannersValidateBeforeCall(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling getBanners(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling getBanners(Async)");
        }
        
        // verify the required parameter 'bannertype' is set
        if (bannertype == null) {
            throw new ApiException("Missing the required parameter 'bannertype' when calling getBanners(Async)");
        }
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling getBanners(Async)");
        }
        
        // verify the required parameter 'accountType' is set
        if (accountType == null) {
            throw new ApiException("Missing the required parameter 'accountType' when calling getBanners(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBannersCall(subdealerid, salespersonid, bannertype, apiKey, accountType, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Get banners
     * Get the current list of banners for given banner_type as specified in the URL
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return BannersOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BannersOutput getBanners(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType) throws ApiException {
        ApiResponse<BannersOutput> resp = getBannersWithHttpInfo(subdealerid, salespersonid, bannertype, apiKey, accountType);
        return resp.getData();
    }

    /**
     * Get banners
     * Get the current list of banners for given banner_type as specified in the URL
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return ApiResponse&lt;BannersOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BannersOutput> getBannersWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  @NotNull @Pattern(regexp="^(?:client|dealer|supervisor)$") String bannertype,  @NotNull @Pattern(regexp="^(ISN)[A-za-z0-9]{3,}$") String apiKey,  @NotNull @Pattern(regexp="^(?:partner|dealer)$") String accountType) throws ApiException {
        com.squareup.okhttp.Call call = getBannersValidateBeforeCall(subdealerid, salespersonid, bannertype, apiKey, accountType, null, null);
        Type localVarReturnType = new TypeToken<BannersOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get banners (asynchronously)
     * Get the current list of banners for given banner_type as specified in the URL
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param bannertype Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBannersAsync(String subdealerid, String salespersonid, String bannertype, String apiKey, String accountType, final ApiCallback<BannersOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getBannersValidateBeforeCall(subdealerid, salespersonid, bannertype, apiKey, accountType, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BannersOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
