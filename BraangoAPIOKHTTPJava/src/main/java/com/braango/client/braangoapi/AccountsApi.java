/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangoapi;

import com.braango.client.ApiCallback;
import com.braango.client.ApiClient;
import com.braango.client.ApiException;
import com.braango.client.ApiResponse;
import com.braango.client.Configuration;
import com.braango.client.Pair;
import com.braango.client.ProgressRequestBody;
import com.braango.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import javax.validation.constraints.*;

import com.braango.client.braangomodel.AccountCreateOutputWrapper;
import com.braango.client.braangomodel.AccountCreateRequestInput;
import com.braango.client.braangomodel.ErrorBadRequestWrapper;
import com.braango.client.braangomodel.ErrorBodyBadAuthorization;
import com.braango.client.braangomodel.ErrorResourceConflictWrapper;
import com.braango.client.braangomodel.ErrorResourceNotFoundWrapper;
import com.braango.client.braangomodel.ErrorResourceNotUpdatedWrapper;
import com.braango.client.braangomodel.ErrorUnprocessableRequestWrapper;
import com.braango.client.braangomodel.ErrorWrapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountsApi {
    private ApiClient apiClient;

    public AccountsApi() {
        this(Configuration.getDefaultApiClient());
    }

    public AccountsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for createPartnerDealer
     * @param body  (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createPartnerDealerCall(AccountCreateRequestInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;
        
        // create path and map variables
        String localVarPath = "/accounts";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createPartnerDealerValidateBeforeCall(AccountCreateRequestInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = createPartnerDealerCall(body, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Create Partner Dealer Account
     * ## Create Account   Use this call to create a **partner dealer** account into the braango system. Typically this could be a real dealership or it could be virtual dealer created by channel partner. The partner dealer account can hold more than 1 sub dealers based on package selected.   &gt;sub dealers are entities that have privileges to add personnel, create supervisors, create groups etc.   &gt;In virual dealer mode, typically sub dealer will be actual business the partner is registering with the braango system.  &gt; In real dealer mode, sub dealer could be a location  &gt; ###### Note sub dealer are actually personnel with full privileges. Typically businesses can use general manager personnel for sub dealer details  Typically based on channel partner&#39;s business model, either channel partner can create a virtual dealer and have all of his clients as sub dealers. This allows for single dashboad managent for partner&#39;s all dealers. Further more, this model allows tighter control over some global resources that channel partner may want to employ. To use this model, please select package \&quot;Enterprise\&quot;. Currently for each virtual dealer, there can be 3000 sub dealers created.  Virtual dealer model is highly recommended for braango leads product. Further more virutal dealer model allows for flat hierarchy --&gt; Virtual Dealer-&gt;Sub Dealer-&gt;Personnel.  For virtual dealer model, each franhcise location will be created as separate sub-dealer  For real dealer model i.e channel partner creating a real dealer account is good for scenarios where franchise dealers want their own hierarchy and single dashboard.  ----------------------------------------  -----------  Account creation process involves following steps  - Creating partnerDealer account into Braango System for generation of API key of this account - Creating default sub dealer for the partner dealer (internally created automatically) - Creating package for this partner dealer account (internally created automatically)  Note each partnerDealer account can have its own braango blast number. This has to be seeded manually at the moment, please contact support@braango.com if your use case needs that
     * @param body  (optional)
     * @return AccountCreateOutputWrapper
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public AccountCreateOutputWrapper createPartnerDealer(AccountCreateRequestInput body) throws ApiException {
        ApiResponse<AccountCreateOutputWrapper> resp = createPartnerDealerWithHttpInfo(body);
        return resp.getData();
    }

    /**
     * Create Partner Dealer Account
     * ## Create Account   Use this call to create a **partner dealer** account into the braango system. Typically this could be a real dealership or it could be virtual dealer created by channel partner. The partner dealer account can hold more than 1 sub dealers based on package selected.   &gt;sub dealers are entities that have privileges to add personnel, create supervisors, create groups etc.   &gt;In virual dealer mode, typically sub dealer will be actual business the partner is registering with the braango system.  &gt; In real dealer mode, sub dealer could be a location  &gt; ###### Note sub dealer are actually personnel with full privileges. Typically businesses can use general manager personnel for sub dealer details  Typically based on channel partner&#39;s business model, either channel partner can create a virtual dealer and have all of his clients as sub dealers. This allows for single dashboad managent for partner&#39;s all dealers. Further more, this model allows tighter control over some global resources that channel partner may want to employ. To use this model, please select package \&quot;Enterprise\&quot;. Currently for each virtual dealer, there can be 3000 sub dealers created.  Virtual dealer model is highly recommended for braango leads product. Further more virutal dealer model allows for flat hierarchy --&gt; Virtual Dealer-&gt;Sub Dealer-&gt;Personnel.  For virtual dealer model, each franhcise location will be created as separate sub-dealer  For real dealer model i.e channel partner creating a real dealer account is good for scenarios where franchise dealers want their own hierarchy and single dashboard.  ----------------------------------------  -----------  Account creation process involves following steps  - Creating partnerDealer account into Braango System for generation of API key of this account - Creating default sub dealer for the partner dealer (internally created automatically) - Creating package for this partner dealer account (internally created automatically)  Note each partnerDealer account can have its own braango blast number. This has to be seeded manually at the moment, please contact support@braango.com if your use case needs that
     * @param body  (optional)
     * @return ApiResponse&lt;AccountCreateOutputWrapper&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<AccountCreateOutputWrapper> createPartnerDealerWithHttpInfo( AccountCreateRequestInput body) throws ApiException {
        com.squareup.okhttp.Call call = createPartnerDealerValidateBeforeCall(body, null, null);
        Type localVarReturnType = new TypeToken<AccountCreateOutputWrapper>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create Partner Dealer Account (asynchronously)
     * ## Create Account   Use this call to create a **partner dealer** account into the braango system. Typically this could be a real dealership or it could be virtual dealer created by channel partner. The partner dealer account can hold more than 1 sub dealers based on package selected.   &gt;sub dealers are entities that have privileges to add personnel, create supervisors, create groups etc.   &gt;In virual dealer mode, typically sub dealer will be actual business the partner is registering with the braango system.  &gt; In real dealer mode, sub dealer could be a location  &gt; ###### Note sub dealer are actually personnel with full privileges. Typically businesses can use general manager personnel for sub dealer details  Typically based on channel partner&#39;s business model, either channel partner can create a virtual dealer and have all of his clients as sub dealers. This allows for single dashboad managent for partner&#39;s all dealers. Further more, this model allows tighter control over some global resources that channel partner may want to employ. To use this model, please select package \&quot;Enterprise\&quot;. Currently for each virtual dealer, there can be 3000 sub dealers created.  Virtual dealer model is highly recommended for braango leads product. Further more virutal dealer model allows for flat hierarchy --&gt; Virtual Dealer-&gt;Sub Dealer-&gt;Personnel.  For virtual dealer model, each franhcise location will be created as separate sub-dealer  For real dealer model i.e channel partner creating a real dealer account is good for scenarios where franchise dealers want their own hierarchy and single dashboard.  ----------------------------------------  -----------  Account creation process involves following steps  - Creating partnerDealer account into Braango System for generation of API key of this account - Creating default sub dealer for the partner dealer (internally created automatically) - Creating package for this partner dealer account (internally created automatically)  Note each partnerDealer account can have its own braango blast number. This has to be seeded manually at the moment, please contact support@braango.com if your use case needs that
     * @param body  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createPartnerDealerAsync(AccountCreateRequestInput body, final ApiCallback<AccountCreateOutputWrapper> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createPartnerDealerValidateBeforeCall(body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<AccountCreateOutputWrapper>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
