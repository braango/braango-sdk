/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangoapi;

import com.braango.client.ApiCallback;
import com.braango.client.ApiClient;
import com.braango.client.ApiException;
import com.braango.client.ApiResponse;
import com.braango.client.Configuration;
import com.braango.client.Pair;
import com.braango.client.ProgressRequestBody;
import com.braango.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import javax.validation.constraints.*;

import com.braango.client.braangomodel.ErrorBadRequestWrapper;
import com.braango.client.braangomodel.ErrorBodyBadAuthorization;
import com.braango.client.braangomodel.ErrorResourceConflictWrapper;
import com.braango.client.braangomodel.ErrorResourceNotFoundWrapper;
import com.braango.client.braangomodel.ErrorResourceNotUpdatedWrapper;
import com.braango.client.braangomodel.ErrorUnprocessableRequestWrapper;
import com.braango.client.braangomodel.ErrorWrapper;
import com.braango.client.braangomodel.GroupInput;
import com.braango.client.braangomodel.GroupOutput;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupsApi {
    private ApiClient apiClient;

    public GroupsApi() {
        this(Configuration.getDefaultApiClient());
    }

    public GroupsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for createGroup
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createGroupCall(String subdealerid, String salespersonid, GroupInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;
        
        // create path and map variables
        String localVarPath = "/personnel/groups/{subdealerid}/{salespersonid}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createGroupValidateBeforeCall(String subdealerid, String salespersonid, GroupInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling createGroup(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling createGroup(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createGroupCall(subdealerid, salespersonid, body, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Create Group
     * Create or subscribe to a group. If the requesting personnel is _sub_dealer_, then a group is created, else personnel is subscribed to a given group
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @return GroupOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public GroupOutput createGroup(String subdealerid, String salespersonid, GroupInput body) throws ApiException {
        ApiResponse<GroupOutput> resp = createGroupWithHttpInfo(subdealerid, salespersonid, body);
        return resp.getData();
    }

    /**
     * Create Group
     * Create or subscribe to a group. If the requesting personnel is _sub_dealer_, then a group is created, else personnel is subscribed to a given group
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @return ApiResponse&lt;GroupOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<GroupOutput> createGroupWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  GroupInput body) throws ApiException {
        com.squareup.okhttp.Call call = createGroupValidateBeforeCall(subdealerid, salespersonid, body, null, null);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create Group (asynchronously)
     * Create or subscribe to a group. If the requesting personnel is _sub_dealer_, then a group is created, else personnel is subscribed to a given group
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param body  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createGroupAsync(String subdealerid, String salespersonid, GroupInput body, final ApiCallback<GroupOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = createGroupValidateBeforeCall(subdealerid, salespersonid, body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteAllGroups
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteAllGroupsCall(String subdealerid, String salespersonid, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/personnel/groups/{subdealerid}/{salespersonid}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (apiKey != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "api_key", apiKey));
        if (accountType != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "account_type", accountType));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteAllGroupsValidateBeforeCall(String subdealerid, String salespersonid, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling deleteAllGroups(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling deleteAllGroups(Async)");
        }
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling deleteAllGroups(Async)");
        }
        
        // verify the required parameter 'accountType' is set
        if (accountType == null) {
            throw new ApiException("Missing the required parameter 'accountType' when calling deleteAllGroups(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteAllGroupsCall(subdealerid, salespersonid, apiKey, accountType, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Delete All Groups
     * This call deletes all the groups associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for each of the group are removed
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return GroupOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public GroupOutput deleteAllGroups(String subdealerid, String salespersonid, String apiKey, String accountType) throws ApiException {
        ApiResponse<GroupOutput> resp = deleteAllGroupsWithHttpInfo(subdealerid, salespersonid, apiKey, accountType);
        return resp.getData();
    }

    /**
     * Delete All Groups
     * This call deletes all the groups associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for each of the group are removed
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return ApiResponse&lt;GroupOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<GroupOutput> deleteAllGroupsWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  @NotNull @Pattern(regexp="^(ISN)[A-za-z0-9]{3,}$") String apiKey,  @NotNull @Pattern(regexp="^(?:partner|dealer)$") String accountType) throws ApiException {
        com.squareup.okhttp.Call call = deleteAllGroupsValidateBeforeCall(subdealerid, salespersonid, apiKey, accountType, null, null);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete All Groups (asynchronously)
     * This call deletes all the groups associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for each of the group are removed
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteAllGroupsAsync(String subdealerid, String salespersonid, String apiKey, String accountType, final ApiCallback<GroupOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteAllGroupsValidateBeforeCall(subdealerid, salespersonid, apiKey, accountType, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteGroup
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param group Group being asked to be deleted (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteGroupCall(String subdealerid, String salespersonid, String group, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/personnel/groups/{subdealerid}/{salespersonid}/{group}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()))
            .replaceAll("\\{" + "group" + "\\}", apiClient.escapeString(group.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (apiKey != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "api_key", apiKey));
        if (accountType != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "account_type", accountType));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteGroupValidateBeforeCall(String subdealerid, String salespersonid, String group, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling deleteGroup(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling deleteGroup(Async)");
        }
        
        // verify the required parameter 'group' is set
        if (group == null) {
            throw new ApiException("Missing the required parameter 'group' when calling deleteGroup(Async)");
        }
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling deleteGroup(Async)");
        }
        
        // verify the required parameter 'accountType' is set
        if (accountType == null) {
            throw new ApiException("Missing the required parameter 'accountType' when calling deleteGroup(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteGroupCall(subdealerid, salespersonid, group, apiKey, accountType, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Delete Group
     * This call deletes  the group specified in the call associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for the group are removed
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param group Group being asked to be deleted (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return GroupOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public GroupOutput deleteGroup(String subdealerid, String salespersonid, String group, String apiKey, String accountType) throws ApiException {
        ApiResponse<GroupOutput> resp = deleteGroupWithHttpInfo(subdealerid, salespersonid, group, apiKey, accountType);
        return resp.getData();
    }

    /**
     * Delete Group
     * This call deletes  the group specified in the call associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for the group are removed
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param group Group being asked to be deleted (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return ApiResponse&lt;GroupOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<GroupOutput> deleteGroupWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  @NotNull String group,  @NotNull @Pattern(regexp="^(ISN)[A-za-z0-9]{3,}$") String apiKey,  @NotNull @Pattern(regexp="^(?:partner|dealer)$") String accountType) throws ApiException {
        com.squareup.okhttp.Call call = deleteGroupValidateBeforeCall(subdealerid, salespersonid, group, apiKey, accountType, null, null);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete Group (asynchronously)
     * This call deletes  the group specified in the call associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for the group are removed
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param group Group being asked to be deleted (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteGroupAsync(String subdealerid, String salespersonid, String group, String apiKey, String accountType, final ApiCallback<GroupOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = deleteGroupValidateBeforeCall(subdealerid, salespersonid, group, apiKey, accountType, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getGroup
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getGroupCall(String subdealerid, String salespersonid, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/personnel/groups/{subdealerid}/{salespersonid}"
            .replaceAll("\\{" + "subdealerid" + "\\}", apiClient.escapeString(subdealerid.toString()))
            .replaceAll("\\{" + "salespersonid" + "\\}", apiClient.escapeString(salespersonid.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (apiKey != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "api_key", apiKey));
        if (accountType != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "account_type", accountType));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getGroupValidateBeforeCall(String subdealerid, String salespersonid, String apiKey, String accountType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'subdealerid' is set
        if (subdealerid == null) {
            throw new ApiException("Missing the required parameter 'subdealerid' when calling getGroup(Async)");
        }
        
        // verify the required parameter 'salespersonid' is set
        if (salespersonid == null) {
            throw new ApiException("Missing the required parameter 'salespersonid' when calling getGroup(Async)");
        }
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling getGroup(Async)");
        }
        
        // verify the required parameter 'accountType' is set
        if (accountType == null) {
            throw new ApiException("Missing the required parameter 'accountType' when calling getGroup(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getGroupCall(subdealerid, salespersonid, apiKey, accountType, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Get Group
     * Get list of groups that this personnel has subscribed to.
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return GroupOutput
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public GroupOutput getGroup(String subdealerid, String salespersonid, String apiKey, String accountType) throws ApiException {
        ApiResponse<GroupOutput> resp = getGroupWithHttpInfo(subdealerid, salespersonid, apiKey, accountType);
        return resp.getData();
    }

    /**
     * Get Group
     * Get list of groups that this personnel has subscribed to.
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @return ApiResponse&lt;GroupOutput&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<GroupOutput> getGroupWithHttpInfo( @NotNull String subdealerid,  @NotNull String salespersonid,  @NotNull @Pattern(regexp="^(ISN)[A-za-z0-9]{3,}$") String apiKey,  @NotNull @Pattern(regexp="^(?:partner|dealer)$") String accountType) throws ApiException {
        com.squareup.okhttp.Call call = getGroupValidateBeforeCall(subdealerid, salespersonid, apiKey, accountType, null, null);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Group (asynchronously)
     * Get list of groups that this personnel has subscribed to.
     * @param subdealerid id of _sub_dealer_ (required)
     * @param salespersonid id of _personnel_ (required)
     * @param apiKey API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time (required)
     * @param accountType Dealer or partner is accessing this API (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getGroupAsync(String subdealerid, String salespersonid, String apiKey, String accountType, final ApiCallback<GroupOutput> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getGroupValidateBeforeCall(subdealerid, salespersonid, apiKey, accountType, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<GroupOutput>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
