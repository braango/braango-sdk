/*
 * braango
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.braango.client.braangoapi;

import com.braango.client.ApiCallback;
import com.braango.client.ApiClient;
import com.braango.client.ApiException;
import com.braango.client.ApiResponse;
import com.braango.client.Configuration;
import com.braango.client.Pair;
import com.braango.client.ProgressRequestBody;
import com.braango.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import javax.validation.constraints.*;

import com.braango.client.braangomodel.ErrorBadRequestWrapper;
import com.braango.client.braangomodel.ErrorBodyBadAuthorization;
import com.braango.client.braangomodel.ErrorResourceConflictWrapper;
import com.braango.client.braangomodel.ErrorResourceNotFoundWrapper;
import com.braango.client.braangomodel.ErrorResourceNotUpdatedWrapper;
import com.braango.client.braangomodel.ErrorUnprocessableRequestWrapper;
import com.braango.client.braangomodel.ErrorWrapper;
import com.braango.client.braangomodel.MessageBlastOutputWrapper;
import com.braango.client.braangomodel.MessageBlastRequestInput;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagingApi {
    private ApiClient apiClient;

    public MessagingApi() {
        this(Configuration.getDefaultApiClient());
    }

    public MessagingApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for messageBlast
     * @param body  (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call messageBlastCall(MessageBlastRequestInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;
        
        // create path and map variables
        String localVarPath = "/communications/messageblast";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "auth_token" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call messageBlastValidateBeforeCall(MessageBlastRequestInput body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = messageBlastCall(body, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Message Blast
     * This API allows partner or dealer to blast messages to one or multiple numbers at the same time. Each person can receive different message  The blasting number is seperate than regular braango number. At this moment that number will be created by Braango Support Team on behest of of partner. Eventually there will be simple API to create blasting number  Partner can choose to blast messages to anyone including dealers  Responses to these messages seeds the client into our database and are sent via REST api call to the partner or master account  Based on response, partner can then decide to bridge the client to particular _sub_dealer_ or _personnel_ within _sub_dealer_ using __dealerconnect__ api  The above described use case of **_late_binding_** mode of operation  Please refer to flow diagrams for various use case flows of this API
     * @param body  (optional)
     * @return MessageBlastOutputWrapper
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public MessageBlastOutputWrapper messageBlast(MessageBlastRequestInput body) throws ApiException {
        ApiResponse<MessageBlastOutputWrapper> resp = messageBlastWithHttpInfo(body);
        return resp.getData();
    }

    /**
     * Message Blast
     * This API allows partner or dealer to blast messages to one or multiple numbers at the same time. Each person can receive different message  The blasting number is seperate than regular braango number. At this moment that number will be created by Braango Support Team on behest of of partner. Eventually there will be simple API to create blasting number  Partner can choose to blast messages to anyone including dealers  Responses to these messages seeds the client into our database and are sent via REST api call to the partner or master account  Based on response, partner can then decide to bridge the client to particular _sub_dealer_ or _personnel_ within _sub_dealer_ using __dealerconnect__ api  The above described use case of **_late_binding_** mode of operation  Please refer to flow diagrams for various use case flows of this API
     * @param body  (optional)
     * @return ApiResponse&lt;MessageBlastOutputWrapper&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<MessageBlastOutputWrapper> messageBlastWithHttpInfo( MessageBlastRequestInput body) throws ApiException {
        com.squareup.okhttp.Call call = messageBlastValidateBeforeCall(body, null, null);
        Type localVarReturnType = new TypeToken<MessageBlastOutputWrapper>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Message Blast (asynchronously)
     * This API allows partner or dealer to blast messages to one or multiple numbers at the same time. Each person can receive different message  The blasting number is seperate than regular braango number. At this moment that number will be created by Braango Support Team on behest of of partner. Eventually there will be simple API to create blasting number  Partner can choose to blast messages to anyone including dealers  Responses to these messages seeds the client into our database and are sent via REST api call to the partner or master account  Based on response, partner can then decide to bridge the client to particular _sub_dealer_ or _personnel_ within _sub_dealer_ using __dealerconnect__ api  The above described use case of **_late_binding_** mode of operation  Please refer to flow diagrams for various use case flows of this API
     * @param body  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call messageBlastAsync(MessageBlastRequestInput body, final ApiCallback<MessageBlastOutputWrapper> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = messageBlastValidateBeforeCall(body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<MessageBlastOutputWrapper>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
