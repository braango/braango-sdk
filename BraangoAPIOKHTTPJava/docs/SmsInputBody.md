
# SmsInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsNumber** | **String** | Cell number where TEXT messages can be received or sent from |  [optional]



