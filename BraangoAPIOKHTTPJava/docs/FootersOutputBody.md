
# FootersOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Determines if the operation was SUCCESS or ended with ERROR. If ERROR, message will have details and MESSAGE-CODE will have detailed error code | 
**message** | **String** | Only valid if status is ERROR |  [optional]
**messageCode** | **String** | Only valid if status is ERROR. This will return the detailed ERROR code |  [optional]
**data** | [**FootersOutputBodyData**](FootersOutputBodyData.md) |  |  [optional]



