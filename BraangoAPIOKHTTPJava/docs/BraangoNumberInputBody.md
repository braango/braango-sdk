
# BraangoNumberInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allPersonnel** | **Boolean** | This value indicates if the braango number to be created is applicable to all personnel that have subscribed to the group for which braango number is being created. Default is all true. | 
**fakeBraangoNumber** | **Boolean** | Set this to 1 when developing application using this API. This will create a mock non functional braango number, but will help test out braango number.  | 
**add** | **Boolean** | This flag indicates whether list of salesPerson provided along with this call is to be added to or deleted from the braangonumber resource that will be created/updated/deleted | 
**salesPersonIdMulti** | [**List&lt;UUID&gt;**](UUID.md) | List of sales personnel that need to be added or removed from this braango number resource | 



