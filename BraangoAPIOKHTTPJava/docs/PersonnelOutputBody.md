
# PersonnelOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PersonnelOutput**](PersonnelOutput.md) |  |  [optional]
**status** | **String** | Indicates if it is SUCESS or WARNING |  [optional]
**message** | **String** | Human readable string of possible warning |  [optional]
**messageCode** | **String** | parse able string in case of warning |  [optional]



