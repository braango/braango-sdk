
# MessageBlast

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numbers** | [**List&lt;MessageBlastNumbers&gt;**](MessageBlastNumbers.md) | Array of numbers that need to be blasted. Each element of array is unique number, with its own message | 



