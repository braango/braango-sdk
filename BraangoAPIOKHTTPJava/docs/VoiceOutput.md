
# VoiceOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  |  [optional]
**body** | [**VoiceOutputBody**](VoiceOutputBody.md) |  |  [optional]



