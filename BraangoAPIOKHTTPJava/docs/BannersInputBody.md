
# BannersInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerBanners** | **List&lt;String&gt;** | Banners that dealer will see on TEXT messages. Braango will randomly choose from one for every message received |  [optional]
**clientBanners** | **List&lt;String&gt;** | Banners that client will see on TEXT messages. Braango will randomly choose from one for every message received |  [optional]
**supervisorBanners** | **List&lt;String&gt;** | Banners that supervisor will see on TEXT messages. Braango will randomly choose from one for every message received |  [optional]



