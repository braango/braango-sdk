# PhonesApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLineType**](PhonesApi.md#getLineType) | **GET** /communications/linetype/{number} | Get Line Type


<a name="getLineType"></a>
# **getLineType**
> LineTypeOutput getLineType(number, apiKey, accountType)

Get Line Type

Given a phone number, determine its line type i.e. check if it is mobile, landline, VOIP etc

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PhonesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PhonesApi apiInstance = new PhonesApi();
String number = "number_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    LineTypeOutput result = apiInstance.getLineType(number, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PhonesApi#getLineType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**LineTypeOutput**](LineTypeOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

