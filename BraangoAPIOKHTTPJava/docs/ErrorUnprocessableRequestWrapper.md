
# ErrorUnprocessableRequestWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  |  [optional]
**body** | [**ErrorBody**](ErrorBody.md) |  |  [optional]



