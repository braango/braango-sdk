
# PersonnelUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **Boolean** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. |  [optional]
**password** | **String** | Password will be encrypted with SHA-25 and base64 encoded and stored internally within the braango system.  |  [optional]
**status** | **String** | Values are Active, Inactive, Vacation |  [optional]
**dealerBanners** | **List&lt;String&gt;** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list |  [optional]
**clientBanners** | **List&lt;String&gt;** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list |  [optional]
**supervisorBanners** | **List&lt;String&gt;** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. |  [optional]
**clientFooters** | **List&lt;String&gt;** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one |  [optional]
**dealerFooters** | **List&lt;String&gt;** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. |  [optional]
**supervisorFooters** | **List&lt;String&gt;** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. |  [optional]
**smsNumber** | **String** | Cell or SMS number of the personnel |  [optional]
**phoneNumber** | **String** | Voice number of the personnel |  [optional]
**email** | **String** | Email that can be used to receive and send text messages |  [optional]
**typeAdfCrm** | **Boolean** | If true, email is of ADF compatible CRM |  [optional]
**group** | **String** | Group that personnel belongs or is attempting to subscribe to  |  [optional]



