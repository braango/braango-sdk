
# SupervisorOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Indicates if request is SUCCES,ERROR or WARNING  | 
**message** | **String** | Human readable ERROR or WARNING message  |  [optional]
**messageCode** | **String** | Machine parse able ERROR or WARNING code  |  [optional]
**data** | [**SupervisorOutputBodyData**](SupervisorOutputBodyData.md) |  |  [optional]



