
# SmsOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**SmsOutputBody**](SmsOutputBody.md) |  |  [optional]



