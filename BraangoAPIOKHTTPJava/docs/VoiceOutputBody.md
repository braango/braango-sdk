
# VoiceOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Either SUCCESS or ERROR or WARNING | 
**messageCode** | **String** | Only valid and present in case of ERROR or WARNING. Note valid value of NULL is permitted. |  [optional]
**message** | **String** | Only valid and present in case of ERROR or WARNING |  [optional]
**data** | [**VoiceOutputBodyData**](VoiceOutputBodyData.md) |  |  [optional]



