
# SupervisorOutputPerGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**SupervisorOutputPerGroupBody**](SupervisorOutputPerGroupBody.md) |  |  [optional]



