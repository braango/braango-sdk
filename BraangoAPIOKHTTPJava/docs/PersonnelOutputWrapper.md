
# PersonnelOutputWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**PersonnelOutputBody**](PersonnelOutputBody.md) |  | 



