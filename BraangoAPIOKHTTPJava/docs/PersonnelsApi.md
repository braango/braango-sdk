# PersonnelsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPersonnel**](PersonnelsApi.md#createPersonnel) | **POST** /personnel/{subdealerid} | Create personnel regular
[**createSubDealer**](PersonnelsApi.md#createSubDealer) | **POST** /personnel | Create SubDealer
[**deletePersonnel**](PersonnelsApi.md#deletePersonnel) | **DELETE** /personnel/{subdealerid}/{salespersonid} | Delete personnel
[**getAllPersonnelForDealer**](PersonnelsApi.md#getAllPersonnelForDealer) | **GET** /personnel | List all personnels
[**getPersonnel**](PersonnelsApi.md#getPersonnel) | **GET** /personnel/{subdealerid}/{salespersonid} | Get personnel
[**getPersonnelsPerSubDealer**](PersonnelsApi.md#getPersonnelsPerSubDealer) | **GET** /personnel/{subdealerid} | List personnels
[**updatePersonnel**](PersonnelsApi.md#updatePersonnel) | **PUT** /personnel/{subdealerid}/{salespersonid} | Update personnel


<a name="createPersonnel"></a>
# **createPersonnel**
> PersonnelOutputWrapper createPersonnel(subdealerid, body)

Create personnel regular

This api call creates the personnel for the current _sub_dealer_.  Personnel is the master resource in braango system. It consists of following sub-resources   * SMS * VOICE * EMAIL/CRM EMAIL * BANNERS * FOOTERS * GROUPS * SUPERVISORS * CRM OBJECT  * WEB HOOK  Either the consumer of this API can create and update entire _personnel_ object or create bits and pieces and update the _sub_resources_ as an when needed. For the benefit of developer, majority of _sub_resouces_ of _personnel_ entity are exposed via API

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
String subdealerid = "subdealerid_example"; // String | 
PersonnelRequestInput body = new PersonnelRequestInput(); // PersonnelRequestInput | 
try {
    PersonnelOutputWrapper result = apiInstance.createPersonnel(subdealerid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#createPersonnel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **body** | [**PersonnelRequestInput**](PersonnelRequestInput.md)|  | [optional]

### Return type

[**PersonnelOutputWrapper**](PersonnelOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createSubDealer"></a>
# **createSubDealer**
> PersonnelOutputWrapper createSubDealer(body)

Create SubDealer

This api call creates the personnel for the current dealer. This call creates sub_dealer_master_personnel i.e. sub dealer master account. This account typically is assigned to Manager of the location or can be abstract entity. However this account has privileges to create groups that other accounts can subscribe too.   Personnel is the master resource in braango system. It consists of following sub-resources   * SMS * VOICE * EMAIL/CRM EMAIL * BANNERS * FOOTERS * GROUPS * SUPERVISORS * CRM OBJECT   Either the consumer of this API can create and update entire _personnel_ object or create bits and pieces and update the _sub_resources_ as an when needed. For the benefit of developer, majority of _sub_resouces_ of _personnel_ entity are exposed via API

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
SubDealerRequestInput body = new SubDealerRequestInput(); // SubDealerRequestInput | 
try {
    PersonnelOutputWrapper result = apiInstance.createSubDealer(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#createSubDealer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SubDealerRequestInput**](SubDealerRequestInput.md)|  | [optional]

### Return type

[**PersonnelOutputWrapper**](PersonnelOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deletePersonnel"></a>
# **deletePersonnel**
> PersonnelOutputWrapper deletePersonnel(subdealerid, salespersonid, apiKey, accountType)

Delete personnel

This api call deletes the personnel that was created under subDealer represented by _subdealerid_.

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
String subdealerid = "subdealerid_example"; // String | Sub dealer for which this sales person belongs to.
String salespersonid = "salespersonid_example"; // String | Sales person ID that was returned when this personnel was created
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    PersonnelOutputWrapper result = apiInstance.deletePersonnel(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#deletePersonnel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| Sub dealer for which this sales person belongs to. |
 **salespersonid** | **String**| Sales person ID that was returned when this personnel was created |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**PersonnelOutputWrapper**](PersonnelOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllPersonnelForDealer"></a>
# **getAllPersonnelForDealer**
> PersonnelOutputListAllSubDealersWrapper getAllPersonnelForDealer(apiKey, accountType)

List all personnels

This api call returns the all personnel that were created under this dealer being accessed via dealer api_key. For virtual dealer model, this will be partner&#39;s api_key (different from auth_token though) and this call will list all the sub-dealers (accounts) and their personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    PersonnelOutputListAllSubDealersWrapper result = apiInstance.getAllPersonnelForDealer(apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#getAllPersonnelForDealer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**PersonnelOutputListAllSubDealersWrapper**](PersonnelOutputListAllSubDealersWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPersonnel"></a>
# **getPersonnel**
> PersonnelOutputWrapper getPersonnel(subdealerid, salespersonid, apiKey, accountType)

Get personnel

This api call returns the personnel that was created under subDealer represented by _subdealerid_. The __ID of the personnel__ is __mandaotry__ path parameter.

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
String subdealerid = "subdealerid_example"; // String | Sub dealer for which this sales person belongs to.
String salespersonid = "salespersonid_example"; // String | Sales person ID that was returned when this personnel was created
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    PersonnelOutputWrapper result = apiInstance.getPersonnel(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#getPersonnel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| Sub dealer for which this sales person belongs to. |
 **salespersonid** | **String**| Sales person ID that was returned when this personnel was created |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**PersonnelOutputWrapper**](PersonnelOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPersonnelsPerSubDealer"></a>
# **getPersonnelsPerSubDealer**
> PersonnelOutputListWrapper getPersonnelsPerSubDealer(subdealerid, apiKey, accountType)

List personnels

This api call returns the all personnel that were created under subDealer represented by _subdealerid_.

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
String subdealerid = "subdealerid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    PersonnelOutputListWrapper result = apiInstance.getPersonnelsPerSubDealer(subdealerid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#getPersonnelsPerSubDealer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**PersonnelOutputListWrapper**](PersonnelOutputListWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updatePersonnel"></a>
# **updatePersonnel**
> PersonnelOutputWrapper updatePersonnel(subdealerid, salespersonid, body)

Update personnel

This api call is used to update personnel for additions. Practically all the fields of the personnel that are necessary for functioning as braango personnel can be updated to add more except for supervisor. Supervisor needs to be updated,created using a seperate API call    __Note__ _This call will be used to only add additional fields. To actually delete unwanted fields, please use appropiate sub_resource calls_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.PersonnelsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

PersonnelsApi apiInstance = new PersonnelsApi();
String subdealerid = "subdealerid_example"; // String | Sub dealer for which this sales person belongs to.
String salespersonid = "salespersonid_example"; // String | Sales person ID that was returned when this personnel was created
PersonnelUpdateRequestInput body = new PersonnelUpdateRequestInput(); // PersonnelUpdateRequestInput | 
try {
    PersonnelOutputWrapper result = apiInstance.updatePersonnel(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonnelsApi#updatePersonnel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| Sub dealer for which this sales person belongs to. |
 **salespersonid** | **String**| Sales person ID that was returned when this personnel was created |
 **body** | [**PersonnelUpdateRequestInput**](PersonnelUpdateRequestInput.md)|  | [optional]

### Return type

[**PersonnelOutputWrapper**](PersonnelOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

