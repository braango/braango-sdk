
# PersonnelOutputSubordinates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **String** |  |  [optional]
**subordinates** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]



