
# AddClient

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientNumber** | **String** | Phone number of client that needs to be connected | 
**connectEnable** | **Boolean** | Flag stating if connection has to enabled or disabled | 
**connectId** | **String** | Unique connection_id . It has be to UUID . In case if you don&#39;t provide one, braango will generate one internally. This is critical to search for connections within the braango system. We recommend to let braango generate one internally.  |  [optional]
**braangoNumber** | **String** | Valid braango number owned by this _sub_dealer_ . Braango will validate this number against the _sub_dealer_  If specified, the connection between client and sub_dealer is enabled using braango_number. When the dealer initiates a message, client will see this braango_number |  [optional]
**message** | **List&lt;String&gt;** | Array of strings. Each line is one entry of array . All the lines get concatenated to form single long message | 
**mediaUrls** | **List&lt;String&gt;** |  |  [optional]
**sendMessage** | **Boolean** | If this flag is false, message will not be sent, only client will be seeded based on send_message_only flag status. Default is false if this flag is not specified |  [optional]
**sendMessageOnly** | **Boolean** | This flag indicates whether  only message needs to be sent based on send_message flag setting or even seeding of client needs to happen. Default is false |  [optional]



