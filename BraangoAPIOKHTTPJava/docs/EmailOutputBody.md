
# EmailOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Indicates if it is SUCCESS,ERROR or WARNING  | 
**data** | [**EmailOutputBodyData**](EmailOutputBodyData.md) |  |  [optional]
**message** | **String** | Human readable ERROR or WARNING message  |  [optional]
**messageCode** | **String** | Machine parse able ERROR or WARNING code  |  [optional]



