
# WebhookOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restEndPoints** | [**List&lt;WebhookOutputBodyDataRestEndPoints&gt;**](WebhookOutputBodyDataRestEndPoints.md) | Array of REST endpoint objects |  [optional]



