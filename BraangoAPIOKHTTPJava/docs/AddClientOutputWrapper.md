
# AddClientOutputWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  |  [optional]
**body** | [**AddClientOutputWrapperBody**](AddClientOutputWrapperBody.md) |  |  [optional]



