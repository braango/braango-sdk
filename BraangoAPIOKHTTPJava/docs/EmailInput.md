
# EmailInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**EmailInputBody**](EmailInputBody.md) |  |  [optional]



