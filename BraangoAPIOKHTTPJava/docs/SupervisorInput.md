
# SupervisorInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**SupervisorInputBody**](SupervisorInputBody.md) |  |  [optional]



