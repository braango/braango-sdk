
# EmailOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**EmailOutputBody**](EmailOutputBody.md) |  |  [optional]



