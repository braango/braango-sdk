
# ErrorResourceNotUpdatedWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  |  [optional]
**body** | [**ErrorResourceNotUpdatedBody**](ErrorResourceNotUpdatedBody.md) |  |  [optional]



