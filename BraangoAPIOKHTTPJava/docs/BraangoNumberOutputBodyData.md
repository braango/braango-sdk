
# BraangoNumberOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**braangoNumberList** | [**List&lt;BraangoNumberOutputBodyDataBraangoNumberList&gt;**](BraangoNumberOutputBodyDataBraangoNumberList.md) | List of Braango number management object |  [optional]



