
# ErrorBodyConflict

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Error due to resource conflict | 
**message** | **String** | Human readable message for conflict | 
**messageCode** | **String** | machine parsable code for conflict | 
**data** | **Object** | Error payload | 



