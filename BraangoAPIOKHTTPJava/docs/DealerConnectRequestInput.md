
# DealerConnectRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**DealerConnect**](DealerConnect.md) |  | 



