
# MessageBlastOutputWrapperBodyDataNumbers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phoneNumber** | **String** | phone_number that was sent message to | 
**numberId** | **String** | reflection of id for this number | 
**dateRequestSent** | **String** | Return will in this format MM/dd/yyyy:hh:mm:aa | 



