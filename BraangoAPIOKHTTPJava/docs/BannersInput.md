
# BannersInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**BannersInputBody**](BannersInputBody.md) |  |  [optional]



