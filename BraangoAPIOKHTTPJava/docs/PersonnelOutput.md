
# PersonnelOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **Boolean** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. | 
**partnerDealer** | **Boolean** | Indicates whether this personnel is partner account or master account for franchise group | 
**dealerApiKey** | **String** | This the dealer&#39;s api key for partner dealer  that has been registered. This key is different from authentication token provided to partner. | 
**dealerId** | **String** | This is the dealer_id assigned to this dealer. For virtual dealer model, this ID will be that of partner dealer that is signing up dealers as sub-dealers. For franchise or hierachical business, this is the dealer ID assigned to the root account or partner dealer account. | 
**dealerPkId** | **Integer** | Integer representation of the dealer ID. This is unique within braango system and gets generated for partner dealer | 
**dealerName** | **String** | Name assigned while signing up this personnel. For _partner dealer_ (master account) this is assigned while creating an account  | 
**salesPersonId** | [**UUID**](UUID.md) | UUID generated internally by &#x60;braango&#x60;  All sub-resources for this personnel are referenced via this ID | 
**personnelName** | **String** | Name assigned to this personnel  | 
**subDealerMasterPersonnel** | **Boolean** | This flag indicates if this is a master account for this sub-dealer. This master account is hierarchically below partner dealer. Typically this will be an account held by manager at location or dealership. This account has some privileges over regular personnel accounts | 
**userName** | **String** | This is a user name created while signing this personnel up. Typically this user name can be used to log into the braango UI. However for whitelabel product, it is expected that this will be used for single signon with respect to dealer account on partner system. i.e. it is expected that partner will pass on the same user name that dealer has on its system to give seamless integration experience. | 
**enabled** | **Boolean** | For account that is active, this will be true.  | 
**privileges** | **String** | This personnel can have privileges of dealer, subDealer or personnel. | 
**groups** | **List&lt;String&gt;** | List of groups this personnel has subscribed to. | 
**emailList** | **List&lt;String&gt;** | List of email address where personnel can receive TEXT messages from clients | 
**smsList** | **List&lt;String&gt;** | List of SMS Enabled phone number where TEXT messages are bridged.  The numbers in this list are in  format. Currently only US and Canada numbers are supported. | 
**voiceList** | **List&lt;String&gt;** | List of VOICE numbers where voice calls need to be bridged  Currently only US and Canada numbers are supported | 
**crmEmailList** | **List&lt;String&gt;** | List of email addresses where leads will be sent in ADF XML format. Note either leads are sent to ADF XML enabled CRMS or other CRMS (base, zoho and pipe) not both | 
**dealerBanners** | **List&lt;String&gt;** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list | 
**clientBanners** | **List&lt;String&gt;** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list | 
**supervisorBanners** | **List&lt;String&gt;** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. | 
**clientFooters** | **List&lt;String&gt;** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one | 
**dealerFooters** | **List&lt;String&gt;** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. | 
**supervisorFooters** | **List&lt;String&gt;** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. | 
**groupsSupervising** | **List&lt;String&gt;** | List of groups that this personnel is supervising. This will be used in future, at present does nothing | 
**supervisors** | [**List&lt;PersonnelOutputSupervisors&gt;**](PersonnelOutputSupervisors.md) | This hash map of list of of supervisors per group that this personnel has subscribed and assigned the supervisor.   &#x60;&#x60;&#x60;java    Map&lt;String,List&lt;String&gt; supervisors | 
**subordinates** | [**List&lt;PersonnelOutputSubordinates&gt;**](PersonnelOutputSubordinates.md) | This hash map of list of of subordinates per group that this personnel has subscribed and is monitoring other personnel in that group.   &#x60;&#x60;&#x60;java    Map&lt;String,List&lt;String&gt; subOrdinates | 



