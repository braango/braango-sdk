
# RequestHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** | API Key for the dealer returned when create_account was called.   | 
**id** | **String** | Id sent bu consumer of this API. This value will be reflected back in response | 
**accountType** | **String** | Account type should be partner for channel partners and integrators ; dealer for dealer access | 



