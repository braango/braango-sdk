
# WebhookOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**WebhookOutputBody**](WebhookOutputBody.md) |  |  [optional]



