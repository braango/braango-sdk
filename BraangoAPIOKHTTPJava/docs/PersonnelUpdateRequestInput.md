
# PersonnelUpdateRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**PersonnelUpdate**](PersonnelUpdate.md) |  | 



