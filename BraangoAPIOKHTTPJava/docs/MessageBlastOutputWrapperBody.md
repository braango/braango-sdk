
# MessageBlastOutputWrapperBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | SUCCESS or ERROR . If ERROR, message-code will indicate class of error and message will describe the error | 
**message** | **String** | Human readable ERROR or WARNING message  |  [optional]
**messageCode** | **String** | Machine parse able ERROR or WARNING code  |  [optional]
**data** | [**MessageBlastOutputWrapperBodyData**](MessageBlastOutputWrapperBodyData.md) |  |  [optional]



