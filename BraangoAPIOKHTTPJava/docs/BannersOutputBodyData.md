
# BannersOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerBanners** | **List&lt;String&gt;** | current dealer banners |  [optional]
**clientBanners** | **List&lt;String&gt;** | current client banners |  [optional]
**supervisorBanners** | **List&lt;String&gt;** | current supervisor banners |  [optional]



