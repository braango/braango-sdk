
# VoiceOutputBodyDataVoiceList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phoneNumber** | **String** | Actual number where voice calls can be received | 
**enable** | **Boolean** | If true voice calls can be received  | 



