
# MessageBlastOutputWrapperBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numbers** | [**List&lt;MessageBlastOutputWrapperBodyDataNumbers&gt;**](MessageBlastOutputWrapperBodyDataNumbers.md) | Return response of request  |  [optional]



