
# MessageBlastNumbers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phoneNumber** | **String** | 10 digit US/Canada number to which the message needs to be sent | 
**numberId** | **String** | This is the id for message request for this number. This is mandatory as it is expected API consumer will use this to track the messages | 
**message** | **List&lt;String&gt;** | TEXT message body. Entry in the array is a line. | 
**mediaUrls** | **List&lt;String&gt;** | List of URLs pointing to video or image. Image has to small size not exceeding 5 MB.  Maximum 8 images or videos can be sent |  [optional]



