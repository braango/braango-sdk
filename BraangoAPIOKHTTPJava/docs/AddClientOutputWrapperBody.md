
# AddClientOutputWrapperBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**messageCode** | **String** |  |  [optional]
**data** | [**AddClientOutputWrapperBodyData**](AddClientOutputWrapperBodyData.md) |  |  [optional]



