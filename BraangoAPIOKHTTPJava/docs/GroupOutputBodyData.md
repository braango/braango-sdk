
# GroupOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groups** | **List&lt;String&gt;** | List of groups that personnel has subscribed to |  [optional]



