# MessagingApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**messageBlast**](MessagingApi.md#messageBlast) | **POST** /communications/messageblast | Message Blast


<a name="messageBlast"></a>
# **messageBlast**
> MessageBlastOutputWrapper messageBlast(body)

Message Blast

This API allows partner or dealer to blast messages to one or multiple numbers at the same time. Each person can receive different message  The blasting number is seperate than regular braango number. At this moment that number will be created by Braango Support Team on behest of of partner. Eventually there will be simple API to create blasting number  Partner can choose to blast messages to anyone including dealers  Responses to these messages seeds the client into our database and are sent via REST api call to the partner or master account  Based on response, partner can then decide to bridge the client to particular _sub_dealer_ or _personnel_ within _sub_dealer_ using __dealerconnect__ api  The above described use case of **_late_binding_** mode of operation  Please refer to flow diagrams for various use case flows of this API

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.MessagingApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

MessagingApi apiInstance = new MessagingApi();
MessageBlastRequestInput body = new MessageBlastRequestInput(); // MessageBlastRequestInput | 
try {
    MessageBlastOutputWrapper result = apiInstance.messageBlast(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MessagingApi#messageBlast");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MessageBlastRequestInput**](MessageBlastRequestInput.md)|  | [optional]

### Return type

[**MessageBlastOutputWrapper**](MessageBlastOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

