
# FootersOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerFooters** | **List&lt;String&gt;** | List of current dealer footers |  [optional]
**clientFooters** | **List&lt;String&gt;** | List of current client footers |  [optional]
**supervisorFooters** | **List&lt;String&gt;** | List of current supervisor footers |  [optional]



