
# BannersOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**BannersOutputBodyData**](BannersOutputBodyData.md) |  |  [optional]
**status** | **String** | Indicates if it is SUCCES,ERROR or WARNING | 
**message** | **String** | Human readable ERROR or WARNING message |  [optional]
**messageCode** | **String** | Machine parse able ERROR or WARNING code |  [optional]



