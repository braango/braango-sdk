
# BannersOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**BannersOutputBody**](BannersOutputBody.md) |  |  [optional]



