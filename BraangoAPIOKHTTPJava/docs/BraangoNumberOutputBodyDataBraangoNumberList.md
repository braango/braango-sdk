
# BraangoNumberOutputBodyDataBraangoNumberList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**braangoNumberId** | [**UUID**](UUID.md) | Internal ID for braango number object |  [optional]
**salesPersonId** | [**UUID**](UUID.md) | personnel owning this braango number |  [optional]
**group** | **String** | Group associated with this braango number |  [optional]
**salesPersonIdMulti** | [**List&lt;UUID&gt;**](UUID.md) | List of personnel associated with this braango number |  [optional]
**braangoNumbers** | **List&lt;String&gt;** | List of braango numbers associated with this group and this object |  [optional]



