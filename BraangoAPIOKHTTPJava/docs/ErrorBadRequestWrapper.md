
# ErrorBadRequestWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**ErrorBodyBadRequest**](ErrorBodyBadRequest.md) |  | 



