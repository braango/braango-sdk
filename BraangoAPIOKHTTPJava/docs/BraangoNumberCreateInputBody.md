
# BraangoNumberCreateInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allPersonnel** | **Boolean** | This value indicates if the braango number to be created is applicable to all personnel that have subscribed to the group for which braango number is being created. __Default__ is all __true__. | 
**fakeBraangoNumber** | **Boolean** | Set this to 1 when developing application using this API. This will create a mock non functional braango number, but will help test out braango number.  | 
**salesPersonIdMulti** | [**List&lt;UUID&gt;**](UUID.md) | List of sales personnel that need to be added . Only if _all_personnel_ flag is false. _all_personnel_ flag overrides this |  [optional]



