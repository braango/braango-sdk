
# PersonnelOutputListAllSubDealersWrapperBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**personnel** | [**List&lt;PersonnelOutput&gt;**](PersonnelOutput.md) |  |  [optional]
**subDealerId** | **String** |  |  [optional]



