
# ErrorResourceConflictWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  |  [optional]
**body** | [**ErrorBodyConflict**](ErrorBodyConflict.md) |  |  [optional]



