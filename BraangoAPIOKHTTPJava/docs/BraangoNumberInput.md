
# BraangoNumberInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**BraangoNumberInputBody**](BraangoNumberInputBody.md) |  |  [optional]



