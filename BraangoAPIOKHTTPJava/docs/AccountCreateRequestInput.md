
# AccountCreateRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**AccountInput**](AccountInput.md) |  | 



