
# MessageBlastOutputWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**MessageBlastOutputWrapperBody**](MessageBlastOutputWrapperBody.md) |  |  [optional]



