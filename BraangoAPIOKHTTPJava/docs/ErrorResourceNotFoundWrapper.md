
# ErrorResourceNotFoundWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  |  [optional]
**body** | [**ErrorBodyNotFound**](ErrorBodyNotFound.md) |  |  [optional]



