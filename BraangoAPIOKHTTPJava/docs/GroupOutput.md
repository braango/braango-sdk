
# GroupOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**GroupOutputBody**](GroupOutputBody.md) |  |  [optional]



