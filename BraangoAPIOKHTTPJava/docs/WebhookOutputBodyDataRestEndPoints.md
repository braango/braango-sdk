
# WebhookOutputBodyDataRestEndPoints

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **Boolean** | If webhook is enabled or not | 
**authKey** | **String** | Authorization key for validating braango webhook requests | 
**authId** | **String** | ID for this webhook | 
**postUrl** | **String** | URL where webhook data can be POSTed | 



