# WebhooksApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createWebhook**](WebhooksApi.md#createWebhook) | **POST** /personnel/webhook/{subdealerid}/{salespersonid} | Create Webhook
[**deleteOneWebhook**](WebhooksApi.md#deleteOneWebhook) | **DELETE** /personnel/webhook/{subdealerid}/{salespersonid}/{authid} | Delete One Webhook
[**deleteWebhook**](WebhooksApi.md#deleteWebhook) | **DELETE** /personnel/webhook/{subdealerid}/{salespersonid} | Delete Webhook
[**getWebhook**](WebhooksApi.md#getWebhook) | **GET** /personnel/webhook/{subdealerid}/{salespersonid} | Get Webhook


<a name="createWebhook"></a>
# **createWebhook**
> WebhookOutput createWebhook(subdealerid, salespersonid, body)

Create Webhook

Use this api call to add a webhook in to the braango system. Each webhook is associated with a personnel to personalize the experierience

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

WebhooksApi apiInstance = new WebhooksApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
WebhookInput body = new WebhookInput(); // WebhookInput | 
try {
    WebhookOutput result = apiInstance.createWebhook(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#createWebhook");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **body** | [**WebhookInput**](WebhookInput.md)|  | [optional]

### Return type

[**WebhookOutput**](WebhookOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteOneWebhook"></a>
# **deleteOneWebhook**
> deleteOneWebhook(subdealerid, salespersonid, authid, apiKey, accountType)

Delete One Webhook

Delete one webhook as speficied by the ID in the URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

WebhooksApi apiInstance = new WebhooksApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String authid = "authid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    apiInstance.deleteOneWebhook(subdealerid, salespersonid, authid, apiKey, accountType);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#deleteOneWebhook");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **authid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

null (empty response body)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteWebhook"></a>
# **deleteWebhook**
> deleteWebhook(subdealerid, salespersonid, apiKey, accountType)

Delete Webhook

Delete all the webhooks for given personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

WebhooksApi apiInstance = new WebhooksApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    apiInstance.deleteWebhook(subdealerid, salespersonid, apiKey, accountType);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#deleteWebhook");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

null (empty response body)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWebhook"></a>
# **getWebhook**
> WebhookOutput getWebhook(subdealerid, salespersonid, apiKey, accountType)

Get Webhook

Get webhooks associated with given _personnel_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.WebhooksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

WebhooksApi apiInstance = new WebhooksApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    WebhookOutput result = apiInstance.getWebhook(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#getWebhook");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**WebhookOutput**](WebhookOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

