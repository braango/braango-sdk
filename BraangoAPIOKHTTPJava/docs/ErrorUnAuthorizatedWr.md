
# ErrorUnAuthorizatedWr

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**ErrorBodyBadAuthorization**](ErrorBodyBadAuthorization.md) |  | 



