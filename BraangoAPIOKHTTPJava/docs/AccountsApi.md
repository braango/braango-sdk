# AccountsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPartnerDealer**](AccountsApi.md#createPartnerDealer) | **POST** /accounts | Create Partner Dealer Account


<a name="createPartnerDealer"></a>
# **createPartnerDealer**
> AccountCreateOutputWrapper createPartnerDealer(body)

Create Partner Dealer Account

## Create Account   Use this call to create a **partner dealer** account into the braango system. Typically this could be a real dealership or it could be virtual dealer created by channel partner. The partner dealer account can hold more than 1 sub dealers based on package selected.   &gt;sub dealers are entities that have privileges to add personnel, create supervisors, create groups etc.   &gt;In virual dealer mode, typically sub dealer will be actual business the partner is registering with the braango system.  &gt; In real dealer mode, sub dealer could be a location  &gt; ###### Note sub dealer are actually personnel with full privileges. Typically businesses can use general manager personnel for sub dealer details  Typically based on channel partner&#39;s business model, either channel partner can create a virtual dealer and have all of his clients as sub dealers. This allows for single dashboad managent for partner&#39;s all dealers. Further more, this model allows tighter control over some global resources that channel partner may want to employ. To use this model, please select package \&quot;Enterprise\&quot;. Currently for each virtual dealer, there can be 3000 sub dealers created.  Virtual dealer model is highly recommended for braango leads product. Further more virutal dealer model allows for flat hierarchy --&gt; Virtual Dealer-&gt;Sub Dealer-&gt;Personnel.  For virtual dealer model, each franhcise location will be created as separate sub-dealer  For real dealer model i.e channel partner creating a real dealer account is good for scenarios where franchise dealers want their own hierarchy and single dashboard.  ----------------------------------------  -----------  Account creation process involves following steps  - Creating partnerDealer account into Braango System for generation of API key of this account - Creating default sub dealer for the partner dealer (internally created automatically) - Creating package for this partner dealer account (internally created automatically)  Note each partnerDealer account can have its own braango blast number. This has to be seeded manually at the moment, please contact support@braango.com if your use case needs that

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.AccountsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

AccountsApi apiInstance = new AccountsApi();
AccountCreateRequestInput body = new AccountCreateRequestInput(); // AccountCreateRequestInput | 
try {
    AccountCreateOutputWrapper result = apiInstance.createPartnerDealer(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountsApi#createPartnerDealer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AccountCreateRequestInput**](AccountCreateRequestInput.md)|  | [optional]

### Return type

[**AccountCreateOutputWrapper**](AccountCreateOutputWrapper.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

