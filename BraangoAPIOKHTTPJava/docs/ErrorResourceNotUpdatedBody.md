
# ErrorResourceNotUpdatedBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Indicates if response is ERROR or WARNING  | 
**message** | **String** | Human readable ERROR or WARNING message  | 
**messageCode** | **String** | Machine parse able ERROR or WARNING code  | 
**data** | **Object** | Error or Warning payload wrapper | 



