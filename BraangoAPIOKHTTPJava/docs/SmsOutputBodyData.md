
# SmsOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsList** | [**List&lt;SmsOutputBodyDataSmsList&gt;**](SmsOutputBodyDataSmsList.md) | List of SMS enabled numbers for this personnel | 



