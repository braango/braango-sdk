
# VoiceInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**VoiceInputBody**](VoiceInputBody.md) |  |  [optional]



