
# FootersInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerFooters** | **List&lt;String&gt;** | Footers that dealer will see. Braango will randomly choose one of these for every TEXT message received and insert it as footer  |  [optional]
**clientFooters** | **List&lt;String&gt;** | Footers that client will see. Braango will randomly choose one of these for every TEXT message received and insert it as footer  |  [optional]
**supervisorFooters** | **List&lt;String&gt;** | Footers that supervisor will see. Braango will randomly choose one of these for every TEXT message received and insert it as footer  |  [optional]



