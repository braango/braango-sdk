
# EmailOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailList** | **List&lt;String&gt;** | List of emails personnel has subscribed to. Will be returned if request had type_adf_crm &#x3D; false, else will be null |  [optional]
**crmEmailList** | **List&lt;String&gt;** | List of CRM emails personnel has subscribed to. Will be returned if request had type_adf_crm &#x3D; true, else will be null |  [optional]



