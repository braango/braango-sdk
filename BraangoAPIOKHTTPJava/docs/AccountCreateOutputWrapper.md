
# AccountCreateOutputWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**AccountCreateOutputWrapperBody**](AccountCreateOutputWrapperBody.md) |  |  [optional]



