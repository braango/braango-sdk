
# LineTypeOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**LineTypeOutputBody**](LineTypeOutputBody.md) |  |  [optional]



