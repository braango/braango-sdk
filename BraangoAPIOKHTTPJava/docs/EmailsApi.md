# EmailsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createEmail**](EmailsApi.md#createEmail) | **POST** /personnel/email/{subdealerid}/{salespersonid} | Create email
[**deleteEmail**](EmailsApi.md#deleteEmail) | **DELETE** /personnel/email/{subdealerid}/{salespersonid} | Delete email
[**deleteOneEmail**](EmailsApi.md#deleteOneEmail) | **DELETE** /personnel/email/{subdealerid}/{salespersonid}/{email} | Delete One email
[**getEmail**](EmailsApi.md#getEmail) | **GET** /personnel/email/{subdealerid}/{salespersonid} | Get email


<a name="createEmail"></a>
# **createEmail**
> EmailOutput createEmail(subdealerid, salespersonid, body)

Create email

Add the email where personnel can receive and send TEXT messages from. If type_adf_crm is set to true, the email represents ADF XML compatible CRM email address

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.EmailsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

EmailsApi apiInstance = new EmailsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | if of _personnel_
EmailInput body = new EmailInput(); // EmailInput | 
try {
    EmailOutput result = apiInstance.createEmail(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#createEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| if of _personnel_ |
 **body** | [**EmailInput**](EmailInput.md)|  | [optional]

### Return type

[**EmailOutput**](EmailOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteEmail"></a>
# **deleteEmail**
> EmailOutput deleteEmail(subdealerid, salespersonid, apiKey, accountType, typeAdfCrm)

Delete email

Delete all emails associated with this personnel.

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.EmailsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

EmailsApi apiInstance = new EmailsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | if of _personnel_
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
Boolean typeAdfCrm = true; // Boolean | Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts
try {
    EmailOutput result = apiInstance.deleteEmail(subdealerid, salespersonid, apiKey, accountType, typeAdfCrm);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#deleteEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| if of _personnel_ |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]
 **typeAdfCrm** | **Boolean**| Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts | [optional]

### Return type

[**EmailOutput**](EmailOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteOneEmail"></a>
# **deleteOneEmail**
> EmailOutput deleteOneEmail(subdealerid, salespersonid, email, apiKey, accountType, typeAdfCrm)

Delete One email

Delete one particular email address as specified in the URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.EmailsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

EmailsApi apiInstance = new EmailsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String email = "email_example"; // String | Email to be deleted
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
Boolean typeAdfCrm = true; // Boolean | Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts
try {
    EmailOutput result = apiInstance.deleteOneEmail(subdealerid, salespersonid, email, apiKey, accountType, typeAdfCrm);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#deleteOneEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **email** | **String**| Email to be deleted |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]
 **typeAdfCrm** | **Boolean**| Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts | [optional]

### Return type

[**EmailOutput**](EmailOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEmail"></a>
# **getEmail**
> EmailOutput getEmail(subdealerid, salespersonid, apiKey, accountType, typeAdfCrm)

Get email

Get the list of emails asscociated with this personnel. Based on if type_adf_crm flag, list returned is of CRM emails or regular emails

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.EmailsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

EmailsApi apiInstance = new EmailsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | if of _personnel_
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
Boolean typeAdfCrm = true; // Boolean | Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts
try {
    EmailOutput result = apiInstance.getEmail(subdealerid, salespersonid, apiKey, accountType, typeAdfCrm);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#getEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| if of _personnel_ |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]
 **typeAdfCrm** | **Boolean**| Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts | [optional]

### Return type

[**EmailOutput**](EmailOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

