
# PersonnelOutputListWrapperBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;PersonnelOutput&gt;**](PersonnelOutput.md) |  | 
**status** | **String** | Indicates if response is SUCCES,ERROR or WARNING  | 
**message** | **String** | Human readable ERROR or WARNING message  |  [optional]
**messageCode** | **String** | Machine parse able ERROR or WARNING code  |  [optional]



