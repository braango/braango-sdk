# BraangonumbersApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBraangoNumber**](BraangonumbersApi.md#createBraangoNumber) | **POST** /braangonumber/{subdealerid}/{group} | Create braangonumber
[**deleteBraangoNumber**](BraangonumbersApi.md#deleteBraangoNumber) | **DELETE** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Delete braangonumber
[**getBraangoNumber**](BraangonumbersApi.md#getBraangoNumber) | **GET** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Get braangonumber
[**getBraangoNumbers**](BraangonumbersApi.md#getBraangoNumbers) | **GET** /braangonumber/{subdealerid} | List braangonumbers
[**updateBraangoNumber**](BraangonumbersApi.md#updateBraangoNumber) | **PUT** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Update braangonumber


<a name="createBraangoNumber"></a>
# **createBraangoNumber**
> BraangoNumberOutput createBraangoNumber(subdealerid, group, body)

Create braangonumber

Create Braango Number for a given group. Either all personnel associated with the group can be added to the braango number or selected few

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BraangonumbersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BraangonumbersApi apiInstance = new BraangonumbersApi();
String subdealerid = "subdealerid_example"; // String | 
String group = "group_example"; // String | 
BraangoNumberCreateInput body = new BraangoNumberCreateInput(); // BraangoNumberCreateInput | 
try {
    BraangoNumberOutput result = apiInstance.createBraangoNumber(subdealerid, group, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BraangonumbersApi#createBraangoNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **group** | **String**|  |
 **body** | [**BraangoNumberCreateInput**](BraangoNumberCreateInput.md)|  | [optional]

### Return type

[**BraangoNumberOutput**](BraangoNumberOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteBraangoNumber"></a>
# **deleteBraangoNumber**
> Object deleteBraangoNumber(subdealerid, braangonumberOrUuid, fakeBraangoNumber, apiKey, accountType)

Delete braangonumber

Delete a given braango number. The number will be released and cannot be used anywhere. This is irrervisble action

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BraangonumbersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BraangonumbersApi apiInstance = new BraangonumbersApi();
String subdealerid = "subdealerid_example"; // String | 
String braangonumberOrUuid = "braangonumberOrUuid_example"; // String | 
Boolean fakeBraangoNumber = true; // Boolean | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    Object result = apiInstance.deleteBraangoNumber(subdealerid, braangonumberOrUuid, fakeBraangoNumber, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BraangonumbersApi#deleteBraangoNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **braangonumberOrUuid** | **String**|  |
 **fakeBraangoNumber** | **Boolean**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

**Object**

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBraangoNumber"></a>
# **getBraangoNumber**
> BraangoNumberOutput getBraangoNumber(subdealerid, braangonumberOrUuid, apiKey, accountType)

Get braangonumber

Get the braango number object as specified either by braango number or UUID of the braango number

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BraangonumbersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BraangonumbersApi apiInstance = new BraangonumbersApi();
String subdealerid = "subdealerid_example"; // String | 
String braangonumberOrUuid = "braangonumberOrUuid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    BraangoNumberOutput result = apiInstance.getBraangoNumber(subdealerid, braangonumberOrUuid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BraangonumbersApi#getBraangoNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **braangonumberOrUuid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**BraangoNumberOutput**](BraangoNumberOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBraangoNumbers"></a>
# **getBraangoNumbers**
> BraangoNumberOutput getBraangoNumbers(subdealerid, apiKey, accountType)

List braangonumbers

List all the braango numbers for given _sub_dealer_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BraangonumbersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BraangonumbersApi apiInstance = new BraangonumbersApi();
String subdealerid = "subdealerid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    BraangoNumberOutput result = apiInstance.getBraangoNumbers(subdealerid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BraangonumbersApi#getBraangoNumbers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**BraangoNumberOutput**](BraangoNumberOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateBraangoNumber"></a>
# **updateBraangoNumber**
> BraangoNumberOutput updateBraangoNumber(subdealerid, braangonumberOrUuid, body)

Update braangonumber

Update the _braango_number_ as specified by UUID or actual number in URL. Either the personnel could be added or deleted as specified by add flag

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BraangonumbersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BraangonumbersApi apiInstance = new BraangonumbersApi();
String subdealerid = "subdealerid_example"; // String | 
String braangonumberOrUuid = "braangonumberOrUuid_example"; // String | 
BraangoNumberInput body = new BraangoNumberInput(); // BraangoNumberInput | 
try {
    BraangoNumberOutput result = apiInstance.updateBraangoNumber(subdealerid, braangonumberOrUuid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BraangonumbersApi#updateBraangoNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **braangonumberOrUuid** | **String**|  |
 **body** | [**BraangoNumberInput**](BraangoNumberInput.md)|  | [optional]

### Return type

[**BraangoNumberOutput**](BraangoNumberOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

