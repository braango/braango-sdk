
# SmsOutputBodyDataSmsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **String** | Actual number | 
**carrier** | **String** | Carrier | 
**usage** | [**UsageEnum**](#UsageEnum) | Usage | 


<a name="UsageEnum"></a>
## Enum: UsageEnum
Name | Value
---- | -----
PERSONAL | &quot;PERSONAL&quot;
BUSINESS | &quot;BUSINESS&quot;



