
# WebhookInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**WebhookInputBody**](WebhookInputBody.md) |  |  [optional]



