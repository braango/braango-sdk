
# WebhookInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **Boolean** |  | 
**postUrl** | **String** | Post back URL. Braango will create POST request to this URL  | 
**authKey** | **String** | API Key or token to use to validate requests. Braango will use this as part of header in its web-hook request. Please see &lt;&gt; for the web hook format | 
**authId** | **String** | ID identifying this resource | 



