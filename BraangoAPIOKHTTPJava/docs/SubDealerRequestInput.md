
# SubDealerRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**SubDealerBody**](SubDealerBody.md) |  | 



