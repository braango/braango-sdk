
# BraangoNumberOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**BraangoNumberOutputBody**](BraangoNumberOutputBody.md) |  |  [optional]



