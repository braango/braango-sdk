
# ErrorBodyBadAuthorization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | ERROR | 
**message** | **String** | Human readable ERROR indicating bad authorization | 
**messageCode** | **String** | Machine parse able ERROR code for authentication and/or authorization error | 
**data** | **Object** | Error payload | 



