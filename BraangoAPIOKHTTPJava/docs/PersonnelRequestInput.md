
# PersonnelRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**PersonnelRequest**](PersonnelRequest.md) |  | 



