
# GroupOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | If operation is success, this will be SUCCESS else ERROR or WARNING | 
**message** | **String** | Valid if stat is ERROR. Human readable error message |  [optional]
**messageCode** | **String** | Valid only if status ERROR, returns ERROR-CODE |  [optional]
**data** | [**GroupOutputBodyData**](GroupOutputBodyData.md) |  |  [optional]



