# ConnectsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addClient**](ConnectsApi.md#addClient) | **POST** /communications/addclient/{subdealerid}/{salespersonid} | Add Client
[**addClientAllPersonnel**](ConnectsApi.md#addClientAllPersonnel) | **POST** /communications/addclient/{subdealerid} | Add Client All Personnel
[**dealerConnect**](ConnectsApi.md#dealerConnect) | **POST** /communications/dealerconnect/{subdealerid}/{salespersonid} | Dealer Connect
[**dealerConnectAllPersonnel**](ConnectsApi.md#dealerConnectAllPersonnel) | **POST** /communications/dealerconnect/{subdealerid} | Dealer Connects


<a name="addClient"></a>
# **addClient**
> addClient(subdealerid, salespersonid, body)

Add Client

This endpoint or api is responsible for seeding the client to a particular _personnel_ within _sub_dealer_. In the same call message could be sent to the client spoofing the dealer  There is an option to use _sub_dealer&#39;s_ _braango_number_ or use partner_dealer&#39;s generic blast nunber. Please work with &#39;support@braango.com&#39; to establish generic blast number for your account. If no _braango_number_ is specified, partner&#39;s blast number is used.  As a fallback, braango_system has  generic numbers to that can be used.  At moment for every partner dealer, blast number is seeded by support team, in V2.1 there will be API support or it will be hidden process   By default, the connection to the _personnel_ is not enabled and it is expected that partner will parse the response to leading question is sent to client and based on that decide to enable or disable the bridge  **_(Note _personnel_ and _sub_dealer_master account_ both need to be enabled and active)_*  The same API can be used to send onlt messge to the seeded client, there is no need for seperate API. This use case would be for any changes detected in partner system that needs to be sent to client, such as offers, or personnel entering message in UI  This API along with Dealer Connect(S) api forms a powerful solution for early binding. Lead Generators and Portals will find this flow most interesting

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.ConnectsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

ConnectsApi apiInstance = new ConnectsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
AddClientRequestInput body = new AddClientRequestInput(); // AddClientRequestInput | 
try {
    apiInstance.addClient(subdealerid, salespersonid, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ConnectsApi#addClient");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **body** | [**AddClientRequestInput**](AddClientRequestInput.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addClientAllPersonnel"></a>
# **addClientAllPersonnel**
> addClientAllPersonnel(subdealerid, body)

Add Client All Personnel

This endpoint or api is responsible for seeding the client to a particular _personnel_ within _sub_dealer_. In the same call message could be sent to the client spoofing the dealer  There is an option to use _sub_dealer&#39;s_ _braango_number_ or use partner_dealer&#39;s generic blast nunber. Please work with &#39;support@braango.com&#39; to establish generic blast number for your account. If no _braango_number_ is specified, partner&#39;s blast number is used.  As a fallback, braango_system has  generic numbers to that can be used.  At moment for every partner dealer, blast number is seeded by support team, in V2.1 there will be API support or it will be hidden process   By default, the connection to the _personnel_ is not enabled and it is expected that partner will parse the response to leading question is sent to client and based on that decide to enable or disable the bridge  **_(Note _personnel_ and _sub_dealer_master account_ both need to be enabled and active)_*  The same API can be used to send onlt messge to the seeded client, there is no need for seperate API. This use case would be for any changes detected in partner system that needs to be sent to client, such as offers, or personnel entering message in UI  This API along with Dealer Connect(S) api forms a powerful solution for early binding. Lead Generators and Portals will find this flow most interesting

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.ConnectsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

ConnectsApi apiInstance = new ConnectsApi();
String subdealerid = "subdealerid_example"; // String | 
AddClientRequestInput body = new AddClientRequestInput(); // AddClientRequestInput | 
try {
    apiInstance.addClientAllPersonnel(subdealerid, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ConnectsApi#addClientAllPersonnel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **body** | [**AddClientRequestInput**](AddClientRequestInput.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="dealerConnect"></a>
# **dealerConnect**
> dealerConnect(subdealerid, salespersonid, body)

Dealer Connect

This API enables connecting un-seeded client to the particular _personnel_ within _sub_dealer_ Calling this API seeds the client to particular _personnel_ within the _sub_dealer_. In case if you want to see all personnel within _sub_dealer_ using the more generic version of the API please refer here #endpoint:BvyCc4QLHkDkfyyAF  The API spoofs the client and sends message to the dealer. Partner or master account is expected to notify the dealer that client in question is seeded and is ready to communicate  API is poweful to use _sub_dealer&#39;s_ braango number to connect client with dealer. This would result in engagement between client and dealer using dealer&#39;s braango number. It would be avisable to notify client in that case of change of  ownership either using **_add_client_** api call prior to maing this call (_add_client_ allows to spoof dealer to client) or using **_message_blast_** api ( #endpoint:jiMrsFjry5Tc2dovS ) and notifying change of ownership in the message. This would engagement experience more smoother. By default client is still engaged with _blast_ number that was used in original **_message_blase_** (#endpoint:jiMrsFjry5Tc2dovS) api call  In case if _sub_dealer&#39;s_ braango number is not used to connect client with dealer, client will be able to communicate using the blast number on which it first received the message. The blast number then acts as bridge between the client and the dealer  It should be noted that , client can be connected to multiple _sub_dealers_ by calling this API call repeatedly with diffent _sub_dealers_ . In case connection has to be made to all the personnel within the _sub_dealer_, then there is variant of this API described below that can be used.   _(Note for connecting clients to dealer with blast number, we allow only 3 connections. In case client needs to be connected to more than 3, we suggest to use __sub_dealer&#39;s braango_number__ instead)_  _Combination of _message_blast_ ( #endpoint:jiMrsFjry5Tc2dovS) and this API is very powerful for _lead_reselling_ , _lead_harvesting_ business model. Lead providers typically use this model in say \&quot;_Buy Here Pay Here BHPH_\&quot; or other scenarios where they have quality harvested leads_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.ConnectsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

ConnectsApi apiInstance = new ConnectsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
DealerConnectRequestInput body = new DealerConnectRequestInput(); // DealerConnectRequestInput | 
try {
    apiInstance.dealerConnect(subdealerid, salespersonid, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ConnectsApi#dealerConnect");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **body** | [**DealerConnectRequestInput**](DealerConnectRequestInput.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="dealerConnectAllPersonnel"></a>
# **dealerConnectAllPersonnel**
> dealerConnectAllPersonnel(subdealerid, body)

Dealer Connects

This API enables connecting un-seeded client to the particular all _personnel_ within _sub_dealer_ Calling this API seeds the client to all _personnel_ within the _sub_dealer_. In case if you want to see one personnel within _sub_dealer_ using the more specialized version of the API describe please refer #endpoint:gnBQGj4qG8sYE4A8z  The API spoofs the client and sends message to the dealer. Partner or master account is expected to notify the dealer that client in question is seeded and is ready to communicate  API is poweful to use _sub_dealer&#39;s_ braango number to connect client with dealer. This would result in engagement between client and dealer using dealer&#39;s braango number. It would be avisable to notify client in that case of change of  ownership either using **_add_client_** api call prior to maing this call (_add_client_ allows to spoof dealer to client) or using **_message_blast_** api ( #endpoint:jiMrsFjry5Tc2dovS ) and notifying change of ownership in the message. This would engagement experience more smoother. By default client is still engaged with _blast_ number that was used in original **_message_blase_** (#endpoint:jiMrsFjry5Tc2dovS) api call  In case if _sub_dealer&#39;s_ braango number is not used to connect client with dealer, client will be able to communicate using the blast number on which it first received the message. The blast number then acts as bridge between the client and the dealer  It should be noted that , client can be connected to multiple _sub_dealers_ by calling this API call repeatedly with diffent _sub_dealers_ . In case connection has to be made to all the personnel within the _sub_dealer_, then there is variant of this API described below that can be used.   _(Note for connecting clients to dealer with blast number, we allow only 3 connections. In case client needs to be connected to more than 3, we suggest to use __sub_dealer&#39;s braango_number__ instead)_  _Combination of _message_blast_ ( #endpoint:jiMrsFjry5Tc2dovS) and this API is very powerful for _lead_reselling_ , _lead_harvesting_ business model. Lead providers typically use this model in say \&quot;_Buy Here Pay Here BHPH_\&quot; or other scenarios where they have quality harvested leads_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.ConnectsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

ConnectsApi apiInstance = new ConnectsApi();
String subdealerid = "subdealerid_example"; // String | id of the _sub_dealer_ 
DealerConnectRequestInput body = new DealerConnectRequestInput(); // DealerConnectRequestInput | 
try {
    apiInstance.dealerConnectAllPersonnel(subdealerid, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ConnectsApi#dealerConnectAllPersonnel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of the _sub_dealer_  |
 **body** | [**DealerConnectRequestInput**](DealerConnectRequestInput.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

