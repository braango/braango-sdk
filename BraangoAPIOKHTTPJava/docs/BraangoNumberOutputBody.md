
# BraangoNumberOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** | More description of error message. This field is conditional and may not be present. |  [optional]
**messageCode** | **String** | This describes the error code. This field will be conditional |  [optional]
**data** | [**BraangoNumberOutputBodyData**](BraangoNumberOutputBodyData.md) |  |  [optional]
**status** | **String** | Status of the API request call | 



