
# ResponseHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isnRequestId** | [**UUID**](UUID.md) |  ID generated for every request by braango system. All requests are logged into the braango database and this is the key for that entry | 
**id** | **String** | This is reflection of id of _request_header&#39;s_ id . | 



