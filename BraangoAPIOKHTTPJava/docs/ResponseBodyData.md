
# ResponseBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Indicates if response is SUCCES,ERROR or WARNING  |  [optional]
**message** | **String** | Human readable ERROR or WARNING message  |  [optional]
**messageCode** | **String** | Machine parse able ERROR or WARNING code  |  [optional]
**data** | **Object** | Payload wrapper |  [optional]



