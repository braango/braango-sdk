# SupervisorsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSupervisor**](SupervisorsApi.md#createSupervisor) | **POST** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Create Supervisor
[**deleteAllSupervisors**](SupervisorsApi.md#deleteAllSupervisors) | **DELETE** /personnel/supervisors/{subdealerid}/{salespersonid} | Delete All Supervisor
[**deleteSupervisor**](SupervisorsApi.md#deleteSupervisor) | **DELETE** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Delete Supervisor
[**getAllSupervisors**](SupervisorsApi.md#getAllSupervisors) | **GET** /personnel/supervisors/{subdealerid}/{salespersonid} | List Supervisors
[**getSupervisor**](SupervisorsApi.md#getSupervisor) | **GET** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Get Supervisor


<a name="createSupervisor"></a>
# **createSupervisor**
> SupervisorOutput createSupervisor(subdealerid, salespersonid, group, body)

Create Supervisor

Add a supervisor for given personnel, given group. Supervisor is another personnel that needs to exist in the system

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SupervisorsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SupervisorsApi apiInstance = new SupervisorsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String group = "group_example"; // String | 
SupervisorInput body = new SupervisorInput(); // SupervisorInput | 
try {
    SupervisorOutput result = apiInstance.createSupervisor(subdealerid, salespersonid, group, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupervisorsApi#createSupervisor");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **group** | **String**|  |
 **body** | [**SupervisorInput**](SupervisorInput.md)|  | [optional]

### Return type

[**SupervisorOutput**](SupervisorOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAllSupervisors"></a>
# **deleteAllSupervisors**
> SupervisorOutput deleteAllSupervisors(subdealerid, salespersonid, apiKey, accountType)

Delete All Supervisor

Delete all supervisors for given personnel, all groups

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SupervisorsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SupervisorsApi apiInstance = new SupervisorsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SupervisorOutput result = apiInstance.deleteAllSupervisors(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupervisorsApi#deleteAllSupervisors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SupervisorOutput**](SupervisorOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteSupervisor"></a>
# **deleteSupervisor**
> SupervisorOutput deleteSupervisor(subdealerid, salespersonid, group, apiKey, accountType)

Delete Supervisor

Delete supervisors for given personnel for given group

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SupervisorsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SupervisorsApi apiInstance = new SupervisorsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String group = "group_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SupervisorOutput result = apiInstance.deleteSupervisor(subdealerid, salespersonid, group, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupervisorsApi#deleteSupervisor");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **group** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SupervisorOutput**](SupervisorOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllSupervisors"></a>
# **getAllSupervisors**
> SupervisorOutput getAllSupervisors(subdealerid, salespersonid, apiKey, accountType)

List Supervisors

Get all supervisors for all groups for given personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SupervisorsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SupervisorsApi apiInstance = new SupervisorsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SupervisorOutput result = apiInstance.getAllSupervisors(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupervisorsApi#getAllSupervisors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SupervisorOutput**](SupervisorOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSupervisor"></a>
# **getSupervisor**
> SupervisorOutput getSupervisor(subdealerid, salespersonid, group, apiKey, accountType)

Get Supervisor

Get list of supervisors for given personnel for given group

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SupervisorsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SupervisorsApi apiInstance = new SupervisorsApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String group = "group_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SupervisorOutput result = apiInstance.getSupervisor(subdealerid, salespersonid, group, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupervisorsApi#getSupervisor");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **group** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SupervisorOutput**](SupervisorOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

