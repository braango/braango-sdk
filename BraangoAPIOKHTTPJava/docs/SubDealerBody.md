
# SubDealerBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **Boolean** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. |  [optional]
**dealerName** | **String** | Name assigned while signing up this personnel. For _partner dealer_ (master account) this is assigned while creating an account  |  [optional]
**personnelName** | **String** | Name assigned to this personnel  | 
**userName** | **String** | This is a user name created while signing this personnel up. Typically this user name can be used to log into the braango UI. However for whitelabel product, it is expected that this will be used for single signon with respect to dealer account on partner system. i.e. it is expected that partner will pass on the same user name that dealer has on its system to give seamless integration experience. | 
**password** | **String** | Password will be encrypted with SHA-25 and base64 encoded and stored internally within the braango system.  | 
**status** | **String** | Values are Active, Inactive, Vacation |  [optional]
**enabled** | **Boolean** | For account that is active, this will be true.  |  [optional]
**dealerBanners** | **List&lt;String&gt;** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list |  [optional]
**clientBanners** | **List&lt;String&gt;** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list |  [optional]
**supervisorBanners** | **List&lt;String&gt;** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. |  [optional]
**clientFooters** | **List&lt;String&gt;** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one |  [optional]
**dealerFooters** | **List&lt;String&gt;** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. |  [optional]
**supervisorFooters** | **List&lt;String&gt;** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. |  [optional]
**email** | **String** | Email id of personnel |  [optional]
**smsNumber** | **String** | Number where dealer&#39;s root account can be reached via SMS |  [optional]
**phoneNumber** | **String** |  |  [optional]
**group** | **String** | Groups this subDealer is subscribed/owner of  |  [optional]
**typeAdfCrm** | **Boolean** | Email being specified is that for ADF XML based CRM |  [optional]
**_package** | **String** |  | 
**subDealerId** | **String** | Sub Dealer ID | 



