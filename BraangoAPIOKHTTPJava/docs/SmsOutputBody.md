
# SmsOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | SUCCESS indicates the operation is successful. If ERROR , then message and message-code will further indicate errors | 
**message** | **String** | Optional, present only in case of ERROR status |  [optional]
**messageCode** | **String** | Optional, present only in case of ERROR status and further indicates the ERROR code |  [optional]
**data** | [**SmsOutputBodyData**](SmsOutputBodyData.md) |  |  [optional]



