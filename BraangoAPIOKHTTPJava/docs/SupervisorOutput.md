
# SupervisorOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**SupervisorOutputBody**](SupervisorOutputBody.md) |  |  [optional]



