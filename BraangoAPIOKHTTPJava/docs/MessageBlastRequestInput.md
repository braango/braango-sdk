
# MessageBlastRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**MessageBlast**](MessageBlast.md) |  | 



