# FootersApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFooters**](FootersApi.md#createFooters) | **POST** /personnel/footers/{subdealerid}/{salespersonid} | Create Footers
[**deleteFooters**](FootersApi.md#deleteFooters) | **DELETE** /personnel/footers/{subdealerid}/{salespersonid}/{footertype} | Delete Footers
[**getFooters**](FootersApi.md#getFooters) | **GET** /personnel/footers/{subdealerid}/{salespersonid}/{footertype} | Get Footers


<a name="createFooters"></a>
# **createFooters**
> FootersOutput createFooters(subdealerid, salespersonid, body)

Create Footers

Create list of footers. All three types of footers can be specified here, client, dealer or supervisor

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.FootersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

FootersApi apiInstance = new FootersApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
FootersInput body = new FootersInput(); // FootersInput | 
try {
    FootersOutput result = apiInstance.createFooters(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FootersApi#createFooters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **body** | [**FootersInput**](FootersInput.md)|  | [optional]

### Return type

[**FootersOutput**](FootersOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteFooters"></a>
# **deleteFooters**
> FootersOutput deleteFooters(subdealerid, salespersonid, footertype, apiKey, accountType)

Delete Footers

Delete footers, footer type needs to be specified in the URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.FootersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

FootersApi apiInstance = new FootersApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String footertype = "footertype_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    FootersOutput result = apiInstance.deleteFooters(subdealerid, salespersonid, footertype, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FootersApi#deleteFooters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **footertype** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**FootersOutput**](FootersOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFooters"></a>
# **getFooters**
> FootersOutput getFooters(subdealerid, salespersonid, footertype, apiKey, accountType)

Get Footers

Get list of footers. Footer type needs to be specified in the URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.FootersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

FootersApi apiInstance = new FootersApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String footertype = "footertype_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    FootersOutput result = apiInstance.getFooters(subdealerid, salespersonid, footertype, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FootersApi#getFooters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **footertype** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**FootersOutput**](FootersOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

