
# PersonnelOutputListAllSubDealersWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**PersonnelOutputListAllSubDealersWrapperBody**](PersonnelOutputListAllSubDealersWrapperBody.md) |  |  [optional]



