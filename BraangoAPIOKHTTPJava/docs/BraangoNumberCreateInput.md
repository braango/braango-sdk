
# BraangoNumberCreateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**BraangoNumberCreateInputBody**](BraangoNumberCreateInputBody.md) |  |  [optional]



