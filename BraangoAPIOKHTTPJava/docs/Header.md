
# Header

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** | API Key for the dealer returned when create_account was called.   | 
**accountType** | **String** | Account type should be partner for channel partners and integrators ; dealer for dealer access | 



