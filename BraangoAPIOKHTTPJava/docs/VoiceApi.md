# VoiceApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVoice**](VoiceApi.md#createVoice) | **POST** /personnel/voice/{subdealerid}/{salespersonid} | Create Voice
[**deleteOneVoice**](VoiceApi.md#deleteOneVoice) | **DELETE** /personnel/voice/{subdealerid}/{salespersonid}/{number} | Delete One Voice
[**deleteVoice**](VoiceApi.md#deleteVoice) | **DELETE** /personnel/voice/{subdealerid}/{salespersonid} | Delete Voice
[**getVoice**](VoiceApi.md#getVoice) | **GET** /personnel/voice/{subdealerid}/{salespersonid} | Get Voice


<a name="createVoice"></a>
# **createVoice**
> VoiceOutput createVoice(subdealerid, salespersonid, body)

Create Voice

Add voice number for the given personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.VoiceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

VoiceApi apiInstance = new VoiceApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
VoiceInput body = new VoiceInput(); // VoiceInput | 
try {
    VoiceOutput result = apiInstance.createVoice(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VoiceApi#createVoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **body** | [**VoiceInput**](VoiceInput.md)|  | [optional]

### Return type

[**VoiceOutput**](VoiceOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteOneVoice"></a>
# **deleteOneVoice**
> VoiceOutput deleteOneVoice(subdealerid, salespersonid, number, apiKey, accountType)

Delete One Voice

Delete one VOICE number associated with this personnel as specified in the URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.VoiceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

VoiceApi apiInstance = new VoiceApi();
String subdealerid = "subdealerid_example"; // String | 
String salespersonid = "salespersonid_example"; // String | 
String number = "number_example"; // String | 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    VoiceOutput result = apiInstance.deleteOneVoice(subdealerid, salespersonid, number, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VoiceApi#deleteOneVoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**|  |
 **salespersonid** | **String**|  |
 **number** | **String**|  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**VoiceOutput**](VoiceOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteVoice"></a>
# **deleteVoice**
> VoiceOutput deleteVoice(subdealerid, salespersonid, apiKey, accountType)

Delete Voice

Delete all VOICE numbers associated with this personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.VoiceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

VoiceApi apiInstance = new VoiceApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    VoiceOutput result = apiInstance.deleteVoice(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VoiceApi#deleteVoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**VoiceOutput**](VoiceOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getVoice"></a>
# **getVoice**
> VoiceOutput getVoice(subdealerid, salespersonid, apiKey, accountType)

Get Voice

Get VOICE numbers associated with this personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.VoiceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

VoiceApi apiInstance = new VoiceApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    VoiceOutput result = apiInstance.getVoice(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VoiceApi#getVoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**VoiceOutput**](VoiceOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

