
# VoiceOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**voiceList** | [**List&lt;VoiceOutputBodyDataVoiceList&gt;**](VoiceOutputBodyDataVoiceList.md) | Array of voice numbers for this personnel |  [optional]



