
# FootersOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**FootersOutputBody**](FootersOutputBody.md) |  |  [optional]



