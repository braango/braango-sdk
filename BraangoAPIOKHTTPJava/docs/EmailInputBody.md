
# EmailInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | Email where personnel can receive or send text messages from | 
**typeAdfCrm** | **Boolean** | If true, email represent ADF compatible CRM | 



