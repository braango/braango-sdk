
# SupervisorOutputPerGroupBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupSupervisors** | [**List&lt;UUID&gt;**](UUID.md) | List of supervisor for given group |  [optional]



