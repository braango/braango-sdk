# SmsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSms**](SmsApi.md#createSms) | **POST** /personnel/SMS/{subdealerid}/{salespersonid} | Create SMS
[**deleteAllSms**](SmsApi.md#deleteAllSms) | **DELETE** /personnel/SMS/{subdealerid}/{salespersonid} | Delete SMS
[**deleteOnesms**](SmsApi.md#deleteOnesms) | **DELETE** /personnel/SMS/{subdealerid}/{salespersonid}/{number} | Delete One SMS
[**readSms**](SmsApi.md#readSms) | **GET** /personnel/SMS/{subdealerid}/{salespersonidstr} | Get SMS


<a name="createSms"></a>
# **createSms**
> SmsOutput createSms(subdealerid, salespersonid, body)

Create SMS

This api call allows to add SMS number for a given _personnel_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SmsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SmsApi apiInstance = new SmsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_account_
String salespersonid = "salespersonid_example"; // String | id of salesperson 
SmsInput body = new SmsInput(); // SmsInput | 
try {
    SmsOutput result = apiInstance.createSms(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SmsApi#createSms");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_account_ |
 **salespersonid** | **String**| id of salesperson  |
 **body** | [**SmsInput**](SmsInput.md)|  | [optional]

### Return type

[**SmsOutput**](SmsOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAllSms"></a>
# **deleteAllSms**
> SmsOutput deleteAllSms(subdealerid, salespersonid, apiKey, accountType)

Delete SMS

Delete all SMS numbers associated with this personnel

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SmsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SmsApi apiInstance = new SmsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_account_
String salespersonid = "salespersonid_example"; // String | id of salesperson 
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SmsOutput result = apiInstance.deleteAllSms(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SmsApi#deleteAllSms");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_account_ |
 **salespersonid** | **String**| id of salesperson  |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SmsOutput**](SmsOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteOnesms"></a>
# **deleteOnesms**
> SmsOutput deleteOnesms(subdealerid, salespersonid, number, apiKey, accountType)

Delete One SMS

Delete SMS number resource provided in URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SmsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SmsApi apiInstance = new SmsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_account_
String salespersonid = "salespersonid_example"; // String | id of _sales_person_
String number = "number_example"; // String | sms number to be deleted
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SmsOutput result = apiInstance.deleteOnesms(subdealerid, salespersonid, number, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SmsApi#deleteOnesms");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_account_ |
 **salespersonid** | **String**| id of _sales_person_ |
 **number** | **String**| sms number to be deleted |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SmsOutput**](SmsOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="readSms"></a>
# **readSms**
> SmsOutput readSms(subdealerid, salespersonidstr, apiKey, accountType)

Get SMS

Get the details of the SMS configured for this _personnel_

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.SmsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

SmsApi apiInstance = new SmsApi();
String subdealerid = "subdealerid_example"; // String | _sub_dealer_ resource that owns this personnel resource
String salespersonidstr = "salespersonidstr_example"; // String | ID of personnel that owns this sub_resource
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    SmsOutput result = apiInstance.readSms(subdealerid, salespersonidstr, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SmsApi#readSms");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| _sub_dealer_ resource that owns this personnel resource |
 **salespersonidstr** | **String**| ID of personnel that owns this sub_resource |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**SmsOutput**](SmsOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

