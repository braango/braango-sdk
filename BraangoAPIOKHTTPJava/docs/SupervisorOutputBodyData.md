
# SupervisorOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supervisors** | [**List&lt;SupervisorOutputBodyDataSupervisors&gt;**](SupervisorOutputBodyDataSupervisors.md) | List of supervisors |  [optional]



