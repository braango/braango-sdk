
# AddClientOutputWrapperBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connectId** | [**UUID**](UUID.md) |  |  [optional]
**connectEnable** | **Boolean** |  |  [optional]



