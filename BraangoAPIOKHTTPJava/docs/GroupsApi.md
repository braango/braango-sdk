# GroupsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createGroup**](GroupsApi.md#createGroup) | **POST** /personnel/groups/{subdealerid}/{salespersonid} | Create Group
[**deleteAllGroups**](GroupsApi.md#deleteAllGroups) | **DELETE** /personnel/groups/{subdealerid}/{salespersonid} | Delete All Groups
[**deleteGroup**](GroupsApi.md#deleteGroup) | **DELETE** /personnel/groups/{subdealerid}/{salespersonid}/{group} | Delete Group
[**getGroup**](GroupsApi.md#getGroup) | **GET** /personnel/groups/{subdealerid}/{salespersonid} | Get Group


<a name="createGroup"></a>
# **createGroup**
> GroupOutput createGroup(subdealerid, salespersonid, body)

Create Group

Create or subscribe to a group. If the requesting personnel is _sub_dealer_, then a group is created, else personnel is subscribed to a given group

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.GroupsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

GroupsApi apiInstance = new GroupsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
GroupInput body = new GroupInput(); // GroupInput | 
try {
    GroupOutput result = apiInstance.createGroup(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupsApi#createGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **body** | [**GroupInput**](GroupInput.md)|  | [optional]

### Return type

[**GroupOutput**](GroupOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAllGroups"></a>
# **deleteAllGroups**
> GroupOutput deleteAllGroups(subdealerid, salespersonid, apiKey, accountType)

Delete All Groups

This call deletes all the groups associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for each of the group are removed

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.GroupsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

GroupsApi apiInstance = new GroupsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    GroupOutput result = apiInstance.deleteAllGroups(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupsApi#deleteAllGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**GroupOutput**](GroupOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteGroup"></a>
# **deleteGroup**
> GroupOutput deleteGroup(subdealerid, salespersonid, group, apiKey, accountType)

Delete Group

This call deletes  the group specified in the call associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for the group are removed

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.GroupsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

GroupsApi apiInstance = new GroupsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String group = "group_example"; // String | Group being asked to be deleted
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    GroupOutput result = apiInstance.deleteGroup(subdealerid, salespersonid, group, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupsApi#deleteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **group** | **String**| Group being asked to be deleted |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**GroupOutput**](GroupOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getGroup"></a>
# **getGroup**
> GroupOutput getGroup(subdealerid, salespersonid, apiKey, accountType)

Get Group

Get list of groups that this personnel has subscribed to.

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.GroupsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

GroupsApi apiInstance = new GroupsApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    GroupOutput result = apiInstance.getGroup(subdealerid, salespersonid, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupsApi#getGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**GroupOutput**](GroupOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

