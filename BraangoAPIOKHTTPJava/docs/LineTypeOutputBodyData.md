
# LineTypeOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lineType** | **String** | Line type : Either mobile, landline, VOIP etc. |  [optional]
**valid** | **Boolean** |  |  [optional]
**phoneNumber** | **String** | Number used for determining the line type |  [optional]



