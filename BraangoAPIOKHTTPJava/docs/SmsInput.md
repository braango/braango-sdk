
# SmsInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**SmsInputBody**](SmsInputBody.md) |  |  [optional]



