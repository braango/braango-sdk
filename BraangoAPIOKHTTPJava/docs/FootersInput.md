
# FootersInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**RequestHeader**](RequestHeader.md) |  | 
**body** | [**FootersInputBody**](FootersInputBody.md) |  |  [optional]



