
# SupervisorInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supervisor** | [**UUID**](UUID.md) | sales person id of _personnel_ that needs to be the supervisor. This is created when calling create personnel (regular) api call | 



