
# SupervisorOutputBodyDataSupervisors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **String** | Group that these supervisors belong to |  [optional]
**supervisors** | [**List&lt;UUID&gt;**](UUID.md) | Actual list of supervisors for the given group |  [optional]



