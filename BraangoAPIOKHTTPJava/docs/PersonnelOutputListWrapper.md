
# PersonnelOutputListWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**PersonnelOutputListWrapperBody**](PersonnelOutputListWrapperBody.md) |  |  [optional]



