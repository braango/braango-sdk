# BannersApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBanners**](BannersApi.md#createBanners) | **POST** /personnel/banners/{subdealerid}/{salespersonid} | Create banners
[**deleteBanners**](BannersApi.md#deleteBanners) | **DELETE** /personnel/banners/{subdealerid}/{salespersonid}/{bannertype} | Delete banners
[**getBanners**](BannersApi.md#getBanners) | **GET** /personnel/banners/{subdealerid}/{salespersonid}/{bannertype} | Get banners


<a name="createBanners"></a>
# **createBanners**
> BannersOutput createBanners(subdealerid, salespersonid, body)

Create banners

Create list of banners. All three types of banners can be specified here, client, dealer or supervisor

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BannersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BannersApi apiInstance = new BannersApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
BannersInput body = new BannersInput(); // BannersInput | 
try {
    BannersOutput result = apiInstance.createBanners(subdealerid, salespersonid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BannersApi#createBanners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **body** | [**BannersInput**](BannersInput.md)|  | [optional]

### Return type

[**BannersOutput**](BannersOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteBanners"></a>
# **deleteBanners**
> BannersOutput deleteBanners(subdealerid, salespersonid, bannertype, apiKey, accountType)

Delete banners

Delete banners by specifying banner type

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BannersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BannersApi apiInstance = new BannersApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String bannertype = "bannertype_example"; // String | Banner type - `client` , `dealer` , `supervisor`
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    BannersOutput result = apiInstance.deleteBanners(subdealerid, salespersonid, bannertype, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BannersApi#deleteBanners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **bannertype** | **String**| Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**BannersOutput**](BannersOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBanners"></a>
# **getBanners**
> BannersOutput getBanners(subdealerid, salespersonid, bannertype, apiKey, accountType)

Get banners

Get the current list of banners for given banner_type as specified in the URL

### Example
```java
// Import classes:
//import com.braango.client.ApiClient;
//import com.braango.client.ApiException;
//import com.braango.client.Configuration;
//import com.braango.client.auth.*;
//import com.braango.client.braangoapi.BannersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: auth_token
ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
auth_token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//auth_token.setApiKeyPrefix("Token");

BannersApi apiInstance = new BannersApi();
String subdealerid = "subdealerid_example"; // String | id of _sub_dealer_
String salespersonid = "salespersonid_example"; // String | id of _personnel_
String bannertype = "bannertype_example"; // String | Banner type - `client` , `dealer` , `supervisor`
String apiKey = "<<api_key>>"; // String | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
String accountType = "<<account_type>>"; // String | Dealer or partner is accessing this API
try {
    BannersOutput result = apiInstance.getBanners(subdealerid, salespersonid, bannertype, apiKey, accountType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BannersApi#getBanners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **String**| id of _sub_dealer_ |
 **salespersonid** | **String**| id of _personnel_ |
 **bannertype** | **String**| Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; |
 **apiKey** | **String**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **String**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**BannersOutput**](BannersOutput.md)

### Authorization

[auth_token](../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

