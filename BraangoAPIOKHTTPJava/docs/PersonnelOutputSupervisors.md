
# PersonnelOutputSupervisors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **String** |  |  [optional]
**supervisors** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]



