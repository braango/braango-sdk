# braangosdk

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.braango</groupId>
    <artifactId>braangosdk</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.braango:braangosdk:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/braangosdk-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.braango.client.*;
import com.braango.client.auth.*;
import com.braango.client.braangomodel.*;
import com.braango.client.braangoapi.AccountsApi;

import java.io.File;
import java.util.*;

public class AccountsApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure API key authorization: auth_token
        ApiKeyAuth auth_token = (ApiKeyAuth) defaultClient.getAuthentication("auth_token");
        auth_token.setApiKey("YOUR API KEY");
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //auth_token.setApiKeyPrefix("Token");

        AccountsApi apiInstance = new AccountsApi();
        AccountCreateRequestInput body = new AccountCreateRequestInput(); // AccountCreateRequestInput | 
        try {
            AccountCreateOutputWrapper result = apiInstance.createPartnerDealer(body);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#createPartnerDealer");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://api.braango.com/v2/braango*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**createPartnerDealer**](docs/AccountsApi.md#createPartnerDealer) | **POST** /accounts | Create Partner Dealer Account
*BannersApi* | [**createBanners**](docs/BannersApi.md#createBanners) | **POST** /personnel/banners/{subdealerid}/{salespersonid} | Create banners
*BannersApi* | [**deleteBanners**](docs/BannersApi.md#deleteBanners) | **DELETE** /personnel/banners/{subdealerid}/{salespersonid}/{bannertype} | Delete banners
*BannersApi* | [**getBanners**](docs/BannersApi.md#getBanners) | **GET** /personnel/banners/{subdealerid}/{salespersonid}/{bannertype} | Get banners
*BraangonumbersApi* | [**createBraangoNumber**](docs/BraangonumbersApi.md#createBraangoNumber) | **POST** /braangonumber/{subdealerid}/{group} | Create braangonumber
*BraangonumbersApi* | [**deleteBraangoNumber**](docs/BraangonumbersApi.md#deleteBraangoNumber) | **DELETE** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Delete braangonumber
*BraangonumbersApi* | [**getBraangoNumber**](docs/BraangonumbersApi.md#getBraangoNumber) | **GET** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Get braangonumber
*BraangonumbersApi* | [**getBraangoNumbers**](docs/BraangonumbersApi.md#getBraangoNumbers) | **GET** /braangonumber/{subdealerid} | List braangonumbers
*BraangonumbersApi* | [**updateBraangoNumber**](docs/BraangonumbersApi.md#updateBraangoNumber) | **PUT** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Update braangonumber
*ConnectsApi* | [**addClient**](docs/ConnectsApi.md#addClient) | **POST** /communications/addclient/{subdealerid}/{salespersonid} | Add Client
*ConnectsApi* | [**addClientAllPersonnel**](docs/ConnectsApi.md#addClientAllPersonnel) | **POST** /communications/addclient/{subdealerid} | Add Client All Personnel
*ConnectsApi* | [**dealerConnect**](docs/ConnectsApi.md#dealerConnect) | **POST** /communications/dealerconnect/{subdealerid}/{salespersonid} | Dealer Connect
*ConnectsApi* | [**dealerConnectAllPersonnel**](docs/ConnectsApi.md#dealerConnectAllPersonnel) | **POST** /communications/dealerconnect/{subdealerid} | Dealer Connects
*EmailsApi* | [**createEmail**](docs/EmailsApi.md#createEmail) | **POST** /personnel/email/{subdealerid}/{salespersonid} | Create email
*EmailsApi* | [**deleteEmail**](docs/EmailsApi.md#deleteEmail) | **DELETE** /personnel/email/{subdealerid}/{salespersonid} | Delete email
*EmailsApi* | [**deleteOneEmail**](docs/EmailsApi.md#deleteOneEmail) | **DELETE** /personnel/email/{subdealerid}/{salespersonid}/{email} | Delete One email
*EmailsApi* | [**getEmail**](docs/EmailsApi.md#getEmail) | **GET** /personnel/email/{subdealerid}/{salespersonid} | Get email
*FootersApi* | [**createFooters**](docs/FootersApi.md#createFooters) | **POST** /personnel/footers/{subdealerid}/{salespersonid} | Create Footers
*FootersApi* | [**deleteFooters**](docs/FootersApi.md#deleteFooters) | **DELETE** /personnel/footers/{subdealerid}/{salespersonid}/{footertype} | Delete Footers
*FootersApi* | [**getFooters**](docs/FootersApi.md#getFooters) | **GET** /personnel/footers/{subdealerid}/{salespersonid}/{footertype} | Get Footers
*GroupsApi* | [**createGroup**](docs/GroupsApi.md#createGroup) | **POST** /personnel/groups/{subdealerid}/{salespersonid} | Create Group
*GroupsApi* | [**deleteAllGroups**](docs/GroupsApi.md#deleteAllGroups) | **DELETE** /personnel/groups/{subdealerid}/{salespersonid} | Delete All Groups
*GroupsApi* | [**deleteGroup**](docs/GroupsApi.md#deleteGroup) | **DELETE** /personnel/groups/{subdealerid}/{salespersonid}/{group} | Delete Group
*GroupsApi* | [**getGroup**](docs/GroupsApi.md#getGroup) | **GET** /personnel/groups/{subdealerid}/{salespersonid} | Get Group
*MessagingApi* | [**messageBlast**](docs/MessagingApi.md#messageBlast) | **POST** /communications/messageblast | Message Blast
*PersonnelsApi* | [**createPersonnel**](docs/PersonnelsApi.md#createPersonnel) | **POST** /personnel/{subdealerid} | Create personnel regular
*PersonnelsApi* | [**createSubDealer**](docs/PersonnelsApi.md#createSubDealer) | **POST** /personnel | Create SubDealer
*PersonnelsApi* | [**deletePersonnel**](docs/PersonnelsApi.md#deletePersonnel) | **DELETE** /personnel/{subdealerid}/{salespersonid} | Delete personnel
*PersonnelsApi* | [**getAllPersonnelForDealer**](docs/PersonnelsApi.md#getAllPersonnelForDealer) | **GET** /personnel | List all personnels
*PersonnelsApi* | [**getPersonnel**](docs/PersonnelsApi.md#getPersonnel) | **GET** /personnel/{subdealerid}/{salespersonid} | Get personnel
*PersonnelsApi* | [**getPersonnelsPerSubDealer**](docs/PersonnelsApi.md#getPersonnelsPerSubDealer) | **GET** /personnel/{subdealerid} | List personnels
*PersonnelsApi* | [**updatePersonnel**](docs/PersonnelsApi.md#updatePersonnel) | **PUT** /personnel/{subdealerid}/{salespersonid} | Update personnel
*PhonesApi* | [**getLineType**](docs/PhonesApi.md#getLineType) | **GET** /communications/linetype/{number} | Get Line Type
*SmsApi* | [**createSms**](docs/SmsApi.md#createSms) | **POST** /personnel/SMS/{subdealerid}/{salespersonid} | Create SMS
*SmsApi* | [**deleteAllSms**](docs/SmsApi.md#deleteAllSms) | **DELETE** /personnel/SMS/{subdealerid}/{salespersonid} | Delete SMS
*SmsApi* | [**deleteOnesms**](docs/SmsApi.md#deleteOnesms) | **DELETE** /personnel/SMS/{subdealerid}/{salespersonid}/{number} | Delete One SMS
*SmsApi* | [**readSms**](docs/SmsApi.md#readSms) | **GET** /personnel/SMS/{subdealerid}/{salespersonidstr} | Get SMS
*SupervisorsApi* | [**createSupervisor**](docs/SupervisorsApi.md#createSupervisor) | **POST** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Create Supervisor
*SupervisorsApi* | [**deleteAllSupervisors**](docs/SupervisorsApi.md#deleteAllSupervisors) | **DELETE** /personnel/supervisors/{subdealerid}/{salespersonid} | Delete All Supervisor
*SupervisorsApi* | [**deleteSupervisor**](docs/SupervisorsApi.md#deleteSupervisor) | **DELETE** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Delete Supervisor
*SupervisorsApi* | [**getAllSupervisors**](docs/SupervisorsApi.md#getAllSupervisors) | **GET** /personnel/supervisors/{subdealerid}/{salespersonid} | List Supervisors
*SupervisorsApi* | [**getSupervisor**](docs/SupervisorsApi.md#getSupervisor) | **GET** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Get Supervisor
*VoiceApi* | [**createVoice**](docs/VoiceApi.md#createVoice) | **POST** /personnel/voice/{subdealerid}/{salespersonid} | Create Voice
*VoiceApi* | [**deleteOneVoice**](docs/VoiceApi.md#deleteOneVoice) | **DELETE** /personnel/voice/{subdealerid}/{salespersonid}/{number} | Delete One Voice
*VoiceApi* | [**deleteVoice**](docs/VoiceApi.md#deleteVoice) | **DELETE** /personnel/voice/{subdealerid}/{salespersonid} | Delete Voice
*VoiceApi* | [**getVoice**](docs/VoiceApi.md#getVoice) | **GET** /personnel/voice/{subdealerid}/{salespersonid} | Get Voice
*WebhooksApi* | [**createWebhook**](docs/WebhooksApi.md#createWebhook) | **POST** /personnel/webhook/{subdealerid}/{salespersonid} | Create Webhook
*WebhooksApi* | [**deleteOneWebhook**](docs/WebhooksApi.md#deleteOneWebhook) | **DELETE** /personnel/webhook/{subdealerid}/{salespersonid}/{authid} | Delete One Webhook
*WebhooksApi* | [**deleteWebhook**](docs/WebhooksApi.md#deleteWebhook) | **DELETE** /personnel/webhook/{subdealerid}/{salespersonid} | Delete Webhook
*WebhooksApi* | [**getWebhook**](docs/WebhooksApi.md#getWebhook) | **GET** /personnel/webhook/{subdealerid}/{salespersonid} | Get Webhook


## Documentation for Models

 - [AccountCreateOutputWrapper](docs/AccountCreateOutputWrapper.md)
 - [AccountCreateOutputWrapperBody](docs/AccountCreateOutputWrapperBody.md)
 - [AccountCreateOutputWrapperBodyData](docs/AccountCreateOutputWrapperBodyData.md)
 - [AccountCreateRequestInput](docs/AccountCreateRequestInput.md)
 - [AccountInput](docs/AccountInput.md)
 - [AddClient](docs/AddClient.md)
 - [AddClientOutputWrapper](docs/AddClientOutputWrapper.md)
 - [AddClientOutputWrapperBody](docs/AddClientOutputWrapperBody.md)
 - [AddClientOutputWrapperBodyData](docs/AddClientOutputWrapperBodyData.md)
 - [AddClientRequestInput](docs/AddClientRequestInput.md)
 - [BannersInput](docs/BannersInput.md)
 - [BannersInputBody](docs/BannersInputBody.md)
 - [BannersOutput](docs/BannersOutput.md)
 - [BannersOutputBody](docs/BannersOutputBody.md)
 - [BannersOutputBodyData](docs/BannersOutputBodyData.md)
 - [BraangoNumberCreateInput](docs/BraangoNumberCreateInput.md)
 - [BraangoNumberCreateInputBody](docs/BraangoNumberCreateInputBody.md)
 - [BraangoNumberInput](docs/BraangoNumberInput.md)
 - [BraangoNumberInputBody](docs/BraangoNumberInputBody.md)
 - [BraangoNumberOutput](docs/BraangoNumberOutput.md)
 - [BraangoNumberOutputBody](docs/BraangoNumberOutputBody.md)
 - [BraangoNumberOutputBodyData](docs/BraangoNumberOutputBodyData.md)
 - [BraangoNumberOutputBodyDataBraangoNumberList](docs/BraangoNumberOutputBodyDataBraangoNumberList.md)
 - [DealerConnect](docs/DealerConnect.md)
 - [DealerConnectRequestInput](docs/DealerConnectRequestInput.md)
 - [EmailInput](docs/EmailInput.md)
 - [EmailInputBody](docs/EmailInputBody.md)
 - [EmailOutput](docs/EmailOutput.md)
 - [EmailOutputBody](docs/EmailOutputBody.md)
 - [EmailOutputBodyData](docs/EmailOutputBodyData.md)
 - [ErrorBadRequestWrapper](docs/ErrorBadRequestWrapper.md)
 - [ErrorBody](docs/ErrorBody.md)
 - [ErrorBodyBadAuthorization](docs/ErrorBodyBadAuthorization.md)
 - [ErrorBodyBadRequest](docs/ErrorBodyBadRequest.md)
 - [ErrorBodyConflict](docs/ErrorBodyConflict.md)
 - [ErrorBodyNotFound](docs/ErrorBodyNotFound.md)
 - [ErrorResourceConflictWrapper](docs/ErrorResourceConflictWrapper.md)
 - [ErrorResourceNotFoundWrapper](docs/ErrorResourceNotFoundWrapper.md)
 - [ErrorResourceNotUpdatedBody](docs/ErrorResourceNotUpdatedBody.md)
 - [ErrorResourceNotUpdatedWrapper](docs/ErrorResourceNotUpdatedWrapper.md)
 - [ErrorUnAuthorizatedWr](docs/ErrorUnAuthorizatedWr.md)
 - [ErrorUnAuthorizedWrapper](docs/ErrorUnAuthorizedWrapper.md)
 - [ErrorUnprocessableRequestWrapper](docs/ErrorUnprocessableRequestWrapper.md)
 - [ErrorWrapper](docs/ErrorWrapper.md)
 - [FootersInput](docs/FootersInput.md)
 - [FootersInputBody](docs/FootersInputBody.md)
 - [FootersOutput](docs/FootersOutput.md)
 - [FootersOutputBody](docs/FootersOutputBody.md)
 - [FootersOutputBodyData](docs/FootersOutputBodyData.md)
 - [GetParams](docs/GetParams.md)
 - [GroupInput](docs/GroupInput.md)
 - [GroupInputBody](docs/GroupInputBody.md)
 - [GroupOutput](docs/GroupOutput.md)
 - [GroupOutputBody](docs/GroupOutputBody.md)
 - [GroupOutputBodyData](docs/GroupOutputBodyData.md)
 - [HdrResponse](docs/HdrResponse.md)
 - [Header](docs/Header.md)
 - [HeaderResponse](docs/HeaderResponse.md)
 - [LineTypeOutput](docs/LineTypeOutput.md)
 - [LineTypeOutputBody](docs/LineTypeOutputBody.md)
 - [LineTypeOutputBodyData](docs/LineTypeOutputBodyData.md)
 - [MessageBlast](docs/MessageBlast.md)
 - [MessageBlastNumbers](docs/MessageBlastNumbers.md)
 - [MessageBlastOutputWrapper](docs/MessageBlastOutputWrapper.md)
 - [MessageBlastOutputWrapperBody](docs/MessageBlastOutputWrapperBody.md)
 - [MessageBlastOutputWrapperBodyData](docs/MessageBlastOutputWrapperBodyData.md)
 - [MessageBlastOutputWrapperBodyDataNumbers](docs/MessageBlastOutputWrapperBodyDataNumbers.md)
 - [MessageBlastRequestInput](docs/MessageBlastRequestInput.md)
 - [PersonnelOutput](docs/PersonnelOutput.md)
 - [PersonnelOutputBody](docs/PersonnelOutputBody.md)
 - [PersonnelOutputListAllSubDealersWrapper](docs/PersonnelOutputListAllSubDealersWrapper.md)
 - [PersonnelOutputListAllSubDealersWrapperBody](docs/PersonnelOutputListAllSubDealersWrapperBody.md)
 - [PersonnelOutputListAllSubDealersWrapperBodyData](docs/PersonnelOutputListAllSubDealersWrapperBodyData.md)
 - [PersonnelOutputListWrapper](docs/PersonnelOutputListWrapper.md)
 - [PersonnelOutputListWrapperBody](docs/PersonnelOutputListWrapperBody.md)
 - [PersonnelOutputSubordinates](docs/PersonnelOutputSubordinates.md)
 - [PersonnelOutputSupervisors](docs/PersonnelOutputSupervisors.md)
 - [PersonnelOutputWrapper](docs/PersonnelOutputWrapper.md)
 - [PersonnelRequest](docs/PersonnelRequest.md)
 - [PersonnelRequestInput](docs/PersonnelRequestInput.md)
 - [PersonnelUpdate](docs/PersonnelUpdate.md)
 - [PersonnelUpdateRequestInput](docs/PersonnelUpdateRequestInput.md)
 - [RequestHeader](docs/RequestHeader.md)
 - [ResponseBody](docs/ResponseBody.md)
 - [ResponseBodyData](docs/ResponseBodyData.md)
 - [ResponseHeader](docs/ResponseHeader.md)
 - [SmsInput](docs/SmsInput.md)
 - [SmsInputBody](docs/SmsInputBody.md)
 - [SmsOutput](docs/SmsOutput.md)
 - [SmsOutputBody](docs/SmsOutputBody.md)
 - [SmsOutputBodyData](docs/SmsOutputBodyData.md)
 - [SmsOutputBodyDataSmsList](docs/SmsOutputBodyDataSmsList.md)
 - [SubDealerBody](docs/SubDealerBody.md)
 - [SubDealerRequestInput](docs/SubDealerRequestInput.md)
 - [SupervisorInput](docs/SupervisorInput.md)
 - [SupervisorInputBody](docs/SupervisorInputBody.md)
 - [SupervisorOutput](docs/SupervisorOutput.md)
 - [SupervisorOutputBody](docs/SupervisorOutputBody.md)
 - [SupervisorOutputBodyData](docs/SupervisorOutputBodyData.md)
 - [SupervisorOutputBodyDataSupervisors](docs/SupervisorOutputBodyDataSupervisors.md)
 - [SupervisorOutputPerGroup](docs/SupervisorOutputPerGroup.md)
 - [SupervisorOutputPerGroupBody](docs/SupervisorOutputPerGroupBody.md)
 - [SupervisorOutputPerGroupBodyData](docs/SupervisorOutputPerGroupBodyData.md)
 - [Test](docs/Test.md)
 - [VoiceInput](docs/VoiceInput.md)
 - [VoiceInputBody](docs/VoiceInputBody.md)
 - [VoiceOutput](docs/VoiceOutput.md)
 - [VoiceOutputBody](docs/VoiceOutputBody.md)
 - [VoiceOutputBodyData](docs/VoiceOutputBodyData.md)
 - [VoiceOutputBodyDataVoiceList](docs/VoiceOutputBodyDataVoiceList.md)
 - [WebhkInput](docs/WebhkInput.md)
 - [WebhookInput](docs/WebhookInput.md)
 - [WebhookInputBody](docs/WebhookInputBody.md)
 - [WebhookOutput](docs/WebhookOutput.md)
 - [WebhookOutputBody](docs/WebhookOutputBody.md)
 - [WebhookOutputBodyData](docs/WebhookOutputBodyData.md)
 - [WebhookOutputBodyDataRestEndPoints](docs/WebhookOutputBodyDataRestEndPoints.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### auth_token

- **Type**: API key
- **API key parameter name**: auth_token
- **Location**: URL query string


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



